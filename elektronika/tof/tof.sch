EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:tof-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Stanowisko laboratoryjne do testowania czujników odległości."
Date "2017-11-07"
Rev "1"
Comp "Politechnika Wrocławska."
Comment1 "Wydział Elektroniki, "
Comment2 "Katedra Cybernetyki i Robotyki,"
Comment3 "Piotr Krzysztof Portasiak, 218594,"
Comment4 ""
$EndDescr
$Comp
L PWR_FLAG #FLG01
U 1 1 5A01041A
P 8800 4550
F 0 "#FLG01" H 8800 4625 50  0001 C CNN
F 1 "PWR_FLAG" H 8800 4700 50  0000 C CNN
F 2 "" H 8800 4550 50  0001 C CNN
F 3 "" H 8800 4550 50  0001 C CNN
	1    8800 4550
	1    0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 5A010444
P 9300 4350
F 0 "#FLG02" H 9300 4425 50  0001 C CNN
F 1 "PWR_FLAG" H 9300 4500 50  0000 C CNN
F 2 "" H 9300 4350 50  0001 C CNN
F 3 "" H 9300 4350 50  0001 C CNN
	1    9300 4350
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5A010461
P 9300 4550
F 0 "#PWR03" H 9300 4300 50  0001 C CNN
F 1 "GND" H 9300 4400 50  0000 C CNN
F 2 "" H 9300 4550 50  0001 C CNN
F 3 "" H 9300 4550 50  0001 C CNN
	1    9300 4550
	-1   0    0    -1  
$EndComp
Text GLabel 8900 4450 2    60   Input ~ 0
3V3
Text GLabel 9400 4450 2    60   Input ~ 0
GND
$Comp
L GND #PWR04
U 1 1 5A0920D8
P 9525 2850
F 0 "#PWR04" H 9525 2600 50  0001 C CNN
F 1 "GND" H 9525 2700 50  0000 C CNN
F 2 "" H 9525 2850 50  0001 C CNN
F 3 "" H 9525 2850 50  0001 C CNN
	1    9525 2850
	-1   0    0    -1  
$EndComp
Text GLabel 9050 2775 2    39   Input ~ 0
SDA
Text GLabel 9050 2875 2    39   Input ~ 0
SCL
Text GLabel 9050 2975 2    39   Input ~ 0
EN1
Text GLabel 9050 3075 2    39   Input ~ 0
INT1
Wire Wire Line
	8800 4350 8800 4550
Wire Wire Line
	9300 4350 9300 4550
Wire Wire Line
	8900 4450 8800 4450
Connection ~ 8800 4450
Wire Wire Line
	9400 4450 9300 4450
Connection ~ 9300 4450
Wire Wire Line
	8950 2575 9200 2575
Wire Wire Line
	9200 2575 9200 2500
Wire Wire Line
	8950 2675 9525 2675
Wire Wire Line
	9525 2675 9525 2850
Wire Wire Line
	9050 2775 8950 2775
Wire Wire Line
	8950 2875 9050 2875
Wire Wire Line
	9050 2975 8950 2975
Wire Wire Line
	8950 3075 9050 3075
$Comp
L Conn_01x07 J4
U 1 1 5A0941CE
P 4550 4425
F 0 "J4" H 4550 4825 50  0000 C CNN
F 1 "VL53L0X" H 4550 4025 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x07_Pitch2.54mm" H 4550 4425 50  0001 C CNN
F 3 "" H 4550 4425 50  0001 C CNN
	1    4550 4425
	-1   0    0    -1  
$EndComp
NoConn ~ 4750 4125
Text GLabel 4850 4225 2    39   Input ~ 0
3V3
Wire Wire Line
	4850 4225 4750 4225
Text GLabel 4850 4325 2    39   Input ~ 0
GND
Wire Wire Line
	4850 4325 4750 4325
Text GLabel 4850 4425 2    39   Input ~ 0
SDA
Wire Wire Line
	4850 4425 4750 4425
Text GLabel 4850 4525 2    39   Input ~ 0
SCL
Wire Wire Line
	4750 4525 4850 4525
Text GLabel 4850 4625 2    39   Input ~ 0
EN1
Wire Wire Line
	4850 4625 4750 4625
Text GLabel 4850 4725 2    39   Input ~ 0
INT1
Wire Wire Line
	4850 4725 4750 4725
Text GLabel 5750 4300 2    39   Input ~ 0
3V3
Wire Wire Line
	5750 4200 5650 4200
Text GLabel 5750 4200 2    39   Input ~ 0
GND
Wire Wire Line
	5750 4300 5650 4300
Text GLabel 5750 4400 2    39   Input ~ 0
SDA
Wire Wire Line
	5750 4400 5650 4400
Text GLabel 5750 4500 2    39   Input ~ 0
SCL
Wire Wire Line
	5650 4500 5750 4500
Text GLabel 5750 4600 2    39   Input ~ 0
EN2
Wire Wire Line
	5750 4600 5650 4600
Text GLabel 5750 4700 2    39   Input ~ 0
INT2
Wire Wire Line
	5750 4700 5650 4700
$Comp
L Conn_01x06 J3
U 1 1 5A0949DB
P 5450 4400
F 0 "J3" H 5450 4700 50  0000 C CNN
F 1 "VL6180X" H 5450 4000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 5450 4400 50  0001 C CNN
F 3 "" H 5450 4400 50  0001 C CNN
	1    5450 4400
	-1   0    0    -1  
$EndComp
Text GLabel 6700 4325 2    39   Input ~ 0
3V3
Wire Wire Line
	6700 4225 6600 4225
Text GLabel 6700 4425 2    39   Input ~ 0
SDA
Wire Wire Line
	6700 4325 6600 4325
Text GLabel 6700 4525 2    39   Input ~ 0
SCL
Wire Wire Line
	6600 4425 6700 4425
Wire Wire Line
	6700 4525 6600 4525
Text GLabel 6700 4625 2    39   Input ~ 0
INT3
Wire Wire Line
	6700 4625 6600 4625
$Comp
L Conn_01x06 J2
U 1 1 5A094D02
P 6400 4325
F 0 "J2" H 6400 4625 50  0000 C CNN
F 1 "APDS-9960" H 6400 3925 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 6400 4325 50  0001 C CNN
F 3 "" H 6400 4325 50  0001 C CNN
	1    6400 4325
	-1   0    0    -1  
$EndComp
NoConn ~ 6600 4125
Text GLabel 6700 4225 2    39   Input ~ 0
ADPS_GND
Wire Wire Line
	5675 3400 5675 3200
Wire Wire Line
	5175 3000 5375 3000
Wire Wire Line
	4975 3000 4875 3000
Wire Wire Line
	5275 3400 5275 3300
Connection ~ 5275 3000
Wire Wire Line
	5275 3100 5275 3000
Wire Wire Line
	5675 2625 5675 2800
$Comp
L R_Small R1
U 1 1 5A0916FB
P 5075 3000
F 0 "R1" H 5105 3020 50  0000 L CNN
F 1 "0R" H 5105 2960 50  0000 L CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 5075 3000 50  0001 C CNN
F 3 "" H 5075 3000 50  0001 C CNN
	1    5075 3000
	0    1    1    0   
$EndComp
$Comp
L GND #PWR05
U 1 1 5A0916F5
P 5675 3400
F 0 "#PWR05" H 5675 3150 50  0001 C CNN
F 1 "GND" H 5675 3250 50  0000 C CNN
F 2 "" H 5675 3400 50  0001 C CNN
F 3 "" H 5675 3400 50  0001 C CNN
	1    5675 3400
	1    0    0    -1  
$EndComp
Text GLabel 5675 2625 1    39   Input ~ 0
ADPS_GND
Text GLabel 4875 3000 0    39   Input ~ 0
EN3
$Comp
L GND #PWR06
U 1 1 5A0916ED
P 5275 3400
F 0 "#PWR06" H 5275 3150 50  0001 C CNN
F 1 "GND" H 5275 3250 50  0000 C CNN
F 2 "" H 5275 3400 50  0001 C CNN
F 3 "" H 5275 3400 50  0001 C CNN
	1    5275 3400
	1    0    0    -1  
$EndComp
$Comp
L R_Small R4
U 1 1 5A0916E7
P 5275 3200
F 0 "R4" H 5305 3220 50  0000 L CNN
F 1 "100K" H 5305 3160 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 5275 3200 50  0001 C CNN
F 3 "" H 5275 3200 50  0001 C CNN
	1    5275 3200
	1    0    0    -1  
$EndComp
$Comp
L Q_NMOS_GSD Q1
U 1 1 5A0916E1
P 5575 3000
F 0 "Q1" H 5775 3050 50  0000 L CNN
F 1 "Q_NMOS_GSD" H 5775 2950 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 5775 3100 50  0001 C CNN
F 3 "" H 5575 3000 50  0001 C CNN
	1    5575 3000
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR07
U 1 1 5A0958AD
P 9200 2500
F 0 "#PWR07" H 9200 2350 50  0001 C CNN
F 1 "+3.3V" H 9200 2640 50  0000 C CNN
F 2 "" H 9200 2500 50  0001 C CNN
F 3 "" H 9200 2500 50  0001 C CNN
	1    9200 2500
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR08
U 1 1 5A095936
P 8800 4350
F 0 "#PWR08" H 8800 4200 50  0001 C CNN
F 1 "+3.3V" H 8800 4490 50  0000 C CNN
F 2 "" H 8800 4350 50  0001 C CNN
F 3 "" H 8800 4350 50  0001 C CNN
	1    8800 4350
	1    0    0    -1  
$EndComp
Text GLabel 9050 3175 2    39   Input ~ 0
EN2
Text GLabel 9050 3275 2    39   Input ~ 0
INT2
Wire Wire Line
	9050 3175 8950 3175
Wire Wire Line
	8950 3275 9050 3275
Text GLabel 9050 3375 2    39   Input ~ 0
EN3
Text GLabel 9050 3475 2    39   Input ~ 0
INT3
Wire Wire Line
	9050 3375 8950 3375
Wire Wire Line
	8950 3475 9050 3475
$Comp
L Conn_01x10 J1
U 1 1 5A095CFC
P 8750 2975
F 0 "J1" H 8750 3475 50  0000 C CNN
F 1 "Conn_01x10" H 8750 2375 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x10_Pitch2.54mm" H 8750 2975 50  0001 C CNN
F 3 "" H 8750 2975 50  0001 C CNN
	1    8750 2975
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V #PWR09
U 1 1 5A096087
P 6750 3000
F 0 "#PWR09" H 6750 2850 50  0001 C CNN
F 1 "+3.3V" H 6750 3140 50  0000 C CNN
F 2 "" H 6750 3000 50  0001 C CNN
F 3 "" H 6750 3000 50  0001 C CNN
	1    6750 3000
	1    0    0    -1  
$EndComp
Text GLabel 6650 3350 0    39   Input ~ 0
SDA
$Comp
L R_Small R2
U 1 1 5A0960C0
P 6750 3175
F 0 "R2" H 6780 3195 50  0000 L CNN
F 1 "4k7" H 6780 3135 50  0000 L CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 6750 3175 50  0001 C CNN
F 3 "" H 6750 3175 50  0001 C CNN
	1    6750 3175
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 3075 6750 3000
Wire Wire Line
	6650 3350 6750 3350
Wire Wire Line
	6750 3350 6750 3275
$Comp
L +3.3V #PWR010
U 1 1 5A096201
P 7375 3000
F 0 "#PWR010" H 7375 2850 50  0001 C CNN
F 1 "+3.3V" H 7375 3140 50  0000 C CNN
F 2 "" H 7375 3000 50  0001 C CNN
F 3 "" H 7375 3000 50  0001 C CNN
	1    7375 3000
	1    0    0    -1  
$EndComp
Text GLabel 7275 3350 0    39   Input ~ 0
SCL
$Comp
L R_Small R3
U 1 1 5A096208
P 7375 3175
F 0 "R3" H 7405 3195 50  0000 L CNN
F 1 "4k7" H 7405 3135 50  0000 L CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 7375 3175 50  0001 C CNN
F 3 "" H 7375 3175 50  0001 C CNN
	1    7375 3175
	1    0    0    -1  
$EndComp
Wire Wire Line
	7375 3075 7375 3000
Wire Wire Line
	7275 3350 7375 3350
Wire Wire Line
	7375 3350 7375 3275
$EndSCHEMATC
