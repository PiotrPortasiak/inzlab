EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:endstop-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Stanowisko laboratoryjne do testowania czujników odległości."
Date "2017-11-07"
Rev "1"
Comp "Politechnika Wrocławska."
Comment1 "Wydział Elektroniki, "
Comment2 "Katedra Cybernetyki i Robotyki,"
Comment3 "Piotr Krzysztof Portasiak, 218594,"
Comment4 ""
$EndDescr
$Comp
L PWR_FLAG #FLG01
U 1 1 5A01041A
P 7300 4300
F 0 "#FLG01" H 7300 4375 50  0001 C CNN
F 1 "PWR_FLAG" H 7300 4450 50  0000 C CNN
F 2 "" H 7300 4300 50  0001 C CNN
F 3 "" H 7300 4300 50  0001 C CNN
	1    7300 4300
	1    0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 5A010444
P 7800 4100
F 0 "#FLG02" H 7800 4175 50  0001 C CNN
F 1 "PWR_FLAG" H 7800 4250 50  0000 C CNN
F 2 "" H 7800 4100 50  0001 C CNN
F 3 "" H 7800 4100 50  0001 C CNN
	1    7800 4100
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5A010461
P 7800 4300
F 0 "#PWR03" H 7800 4050 50  0001 C CNN
F 1 "GND" H 7800 4150 50  0000 C CNN
F 2 "" H 7800 4300 50  0001 C CNN
F 3 "" H 7800 4300 50  0001 C CNN
	1    7800 4300
	-1   0    0    -1  
$EndComp
Text GLabel 7400 4200 2    60   Input ~ 0
5V
Text GLabel 7900 4200 2    60   Input ~ 0
GND
$Comp
L +5V #PWR04
U 1 1 5A0A354E
P 7300 4100
F 0 "#PWR04" H 7300 3950 50  0001 C CNN
F 1 "+5V" H 7300 4240 50  0000 C CNN
F 2 "" H 7300 4100 50  0001 C CNN
F 3 "" H 7300 4100 50  0001 C CNN
	1    7300 4100
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x03 J1
U 1 1 5A0A3675
P 8650 3525
F 0 "J1" H 8650 3725 50  0000 C CNN
F 1 "ENDSTOP" H 8650 3325 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x03_Pitch2.54mm" H 8650 3525 50  0001 C CNN
F 3 "" H 8650 3525 50  0001 C CNN
	1    8650 3525
	-1   0    0    -1  
$EndComp
Text GLabel 8950 3625 2    39   Input ~ 0
END
Wire Wire Line
	7300 4100 7300 4300
Wire Wire Line
	7800 4100 7800 4300
Wire Wire Line
	7400 4200 7300 4200
Connection ~ 7300 4200
Wire Wire Line
	7900 4200 7800 4200
Connection ~ 7800 4200
Wire Wire Line
	8950 3625 8850 3625
Text GLabel 8950 3525 2    39   Input ~ 0
GND
Text GLabel 8950 3425 2    39   Input ~ 0
5V
Wire Wire Line
	8950 3425 8850 3425
Wire Wire Line
	8850 3525 8950 3525
$Comp
L LED_ALT D2
U 1 1 5A0A3862
P 5525 3500
F 0 "D2" H 5525 3600 50  0000 C CNN
F 1 "IR" H 5525 3400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 5525 3500 50  0001 C CNN
F 3 "" H 5525 3500 50  0001 C CNN
	1    5525 3500
	0    -1   -1   0   
$EndComp
$Comp
L D_Photo_ALT D1
U 1 1 5A0A388B
P 5975 3475
F 0 "D1" H 5995 3545 50  0000 L CNN
F 1 "FT" H 5935 3365 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 5925 3475 50  0001 C CNN
F 3 "" H 5925 3475 50  0001 C CNN
	1    5975 3475
	0    -1   -1   0   
$EndComp
$Comp
L R_Small R1
U 1 1 5A0A3944
P 5525 3150
F 0 "R1" H 5555 3170 50  0000 L CNN
F 1 "180R" H 5555 3110 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 5525 3150 50  0001 C CNN
F 3 "" H 5525 3150 50  0001 C CNN
	1    5525 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5525 3250 5525 3350
$Comp
L +5V #PWR05
U 1 1 5A0A3B04
P 5525 2925
F 0 "#PWR05" H 5525 2775 50  0001 C CNN
F 1 "+5V" H 5525 3065 50  0000 C CNN
F 2 "" H 5525 2925 50  0001 C CNN
F 3 "" H 5525 2925 50  0001 C CNN
	1    5525 2925
	1    0    0    -1  
$EndComp
Wire Wire Line
	5525 2925 5525 3050
$Comp
L R_Small R2
U 1 1 5A0A3C1B
P 5975 3150
F 0 "R2" H 6005 3170 50  0000 L CNN
F 1 "2k2" H 6005 3110 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 5975 3150 50  0001 C CNN
F 3 "" H 5975 3150 50  0001 C CNN
	1    5975 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5975 3250 5975 3375
$Comp
L +5V #PWR06
U 1 1 5A0A3C83
P 5975 2925
F 0 "#PWR06" H 5975 2775 50  0001 C CNN
F 1 "+5V" H 5975 3065 50  0000 C CNN
F 2 "" H 5975 2925 50  0001 C CNN
F 3 "" H 5975 2925 50  0001 C CNN
	1    5975 2925
	1    0    0    -1  
$EndComp
Wire Wire Line
	5975 2925 5975 3050
$Comp
L GND #PWR07
U 1 1 5A0A3D39
P 5525 3775
F 0 "#PWR07" H 5525 3525 50  0001 C CNN
F 1 "GND" H 5525 3625 50  0000 C CNN
F 2 "" H 5525 3775 50  0001 C CNN
F 3 "" H 5525 3775 50  0001 C CNN
	1    5525 3775
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5525 3775 5525 3650
$Comp
L GND #PWR08
U 1 1 5A0A3D71
P 5975 3775
F 0 "#PWR08" H 5975 3525 50  0001 C CNN
F 1 "GND" H 5975 3625 50  0000 C CNN
F 2 "" H 5975 3775 50  0001 C CNN
F 3 "" H 5975 3775 50  0001 C CNN
	1    5975 3775
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5975 3775 5975 3675
Text GLabel 6225 3325 2    39   Input ~ 0
END
Wire Wire Line
	6225 3325 5975 3325
Connection ~ 5975 3325
$EndSCHEMATC
