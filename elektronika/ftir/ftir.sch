EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ftir-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Stanowisko laboratoryjne do testowania czujników odległości."
Date "2017-11-07"
Rev "1"
Comp "Politechnika Wrocławska."
Comment1 "Wydział Elektroniki, "
Comment2 "Katedra Cybernetyki i Robotyki,"
Comment3 "Piotr Krzysztof Portasiak, 218594,"
Comment4 ""
$EndDescr
$Comp
L PWR_FLAG #FLG01
U 1 1 5A01041A
P 9800 4800
F 0 "#FLG01" H 9800 4875 50  0001 C CNN
F 1 "PWR_FLAG" H 9800 4950 50  0000 C CNN
F 2 "" H 9800 4800 50  0001 C CNN
F 3 "" H 9800 4800 50  0001 C CNN
	1    9800 4800
	1    0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 5A010444
P 10300 4600
F 0 "#FLG02" H 10300 4675 50  0001 C CNN
F 1 "PWR_FLAG" H 10300 4750 50  0000 C CNN
F 2 "" H 10300 4600 50  0001 C CNN
F 3 "" H 10300 4600 50  0001 C CNN
	1    10300 4600
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5A010461
P 10300 4800
F 0 "#PWR03" H 10300 4550 50  0001 C CNN
F 1 "GND" H 10300 4650 50  0000 C CNN
F 2 "" H 10300 4800 50  0001 C CNN
F 3 "" H 10300 4800 50  0001 C CNN
	1    10300 4800
	-1   0    0    -1  
$EndComp
$Comp
L +5V #PWR04
U 1 1 5A010484
P 9800 4600
F 0 "#PWR04" H 9800 4450 50  0001 C CNN
F 1 "+5V" H 9800 4740 50  0000 C CNN
F 2 "" H 9800 4600 50  0001 C CNN
F 3 "" H 9800 4600 50  0001 C CNN
	1    9800 4600
	-1   0    0    -1  
$EndComp
$Comp
L LM1117-3.3 U2
U 1 1 5A087BEF
P 2175 2150
F 0 "U2" H 2025 2275 50  0000 C CNN
F 1 "LM1117-3.3" H 2175 2275 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:TO-252-2" H 2175 2150 50  0001 C CNN
F 3 "" H 2175 2150 50  0001 C CNN
	1    2175 2150
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR05
U 1 1 5A087D9B
P 1675 1950
F 0 "#PWR05" H 1675 1800 50  0001 C CNN
F 1 "+5V" H 1675 2090 50  0000 C CNN
F 2 "" H 1675 1950 50  0001 C CNN
F 3 "" H 1675 1950 50  0001 C CNN
	1    1675 1950
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR06
U 1 1 5A087DC9
P 2700 1950
F 0 "#PWR06" H 2700 1800 50  0001 C CNN
F 1 "+3.3V" H 2700 2090 50  0000 C CNN
F 2 "" H 2700 1950 50  0001 C CNN
F 3 "" H 2700 1950 50  0001 C CNN
	1    2700 1950
	1    0    0    -1  
$EndComp
$Comp
L CP_Small C3
U 1 1 5A087DF7
P 1675 2375
F 0 "C3" H 1685 2445 50  0000 L CNN
F 1 "10uF" H 1685 2295 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 1675 2375 50  0001 C CNN
F 3 "" H 1675 2375 50  0001 C CNN
	1    1675 2375
	1    0    0    -1  
$EndComp
$Comp
L CP_Small C4
U 1 1 5A088357
P 2700 2375
F 0 "C4" H 2710 2445 50  0000 L CNN
F 1 "10uF" H 2710 2295 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 2700 2375 50  0001 C CNN
F 3 "" H 2700 2375 50  0001 C CNN
	1    2700 2375
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 5A08843D
P 1675 2575
F 0 "#PWR07" H 1675 2325 50  0001 C CNN
F 1 "GND" H 1675 2425 50  0000 C CNN
F 2 "" H 1675 2575 50  0001 C CNN
F 3 "" H 1675 2575 50  0001 C CNN
	1    1675 2575
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 5A088516
P 2700 2575
F 0 "#PWR08" H 2700 2325 50  0001 C CNN
F 1 "GND" H 2700 2425 50  0000 C CNN
F 2 "" H 2700 2575 50  0001 C CNN
F 3 "" H 2700 2575 50  0001 C CNN
	1    2700 2575
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 5A08870F
P 2175 2575
F 0 "#PWR09" H 2175 2325 50  0001 C CNN
F 1 "GND" H 2175 2425 50  0000 C CNN
F 2 "" H 2175 2575 50  0001 C CNN
F 3 "" H 2175 2575 50  0001 C CNN
	1    2175 2575
	1    0    0    -1  
$EndComp
$Comp
L Q_NMOS_GSD Q1
U 1 1 5A088B9B
P 4950 1900
F 0 "Q1" H 5150 1950 50  0000 L CNN
F 1 "Q_NMOS_GSD" H 5150 1850 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 5150 2000 50  0001 C CNN
F 3 "" H 4950 1900 50  0001 C CNN
	1    4950 1900
	1    0    0    -1  
$EndComp
$Comp
L Q_PMOS_GSD Q3
U 1 1 5A088BB8
P 1300 4125
F 0 "Q3" H 1500 4175 50  0000 L CNN
F 1 "Q_PMOS_GSD" H 1500 4075 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 1500 4225 50  0001 C CNN
F 3 "" H 1300 4125 50  0001 C CNN
	1    1300 4125
	0    -1   -1   0   
$EndComp
$Comp
L R_Small R1
U 1 1 5A088E29
P 4650 2100
F 0 "R1" H 4680 2120 50  0000 L CNN
F 1 "100K" H 4680 2060 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 4650 2100 50  0001 C CNN
F 3 "" H 4650 2100 50  0001 C CNN
	1    4650 2100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 5A088FAF
P 4650 2300
F 0 "#PWR010" H 4650 2050 50  0001 C CNN
F 1 "GND" H 4650 2150 50  0000 C CNN
F 2 "" H 4650 2300 50  0001 C CNN
F 3 "" H 4650 2300 50  0001 C CNN
	1    4650 2300
	1    0    0    -1  
$EndComp
Text GLabel 4250 1900 0    39   Input ~ 0
PWM1
$Comp
L C_Small C1
U 1 1 5A089201
P 4450 1900
F 0 "C1" H 4460 1970 50  0000 L CNN
F 1 "100n" H 4460 1820 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4450 1900 50  0001 C CNN
F 3 "" H 4450 1900 50  0001 C CNN
	1    4450 1900
	0    -1   -1   0   
$EndComp
Text GLabel 5050 1175 1    39   Input ~ 0
REG
$Comp
L GND #PWR011
U 1 1 5A0894CE
P 5050 2300
F 0 "#PWR011" H 5050 2050 50  0001 C CNN
F 1 "GND" H 5050 2150 50  0000 C CNN
F 2 "" H 5050 2300 50  0001 C CNN
F 3 "" H 5050 2300 50  0001 C CNN
	1    5050 2300
	1    0    0    -1  
$EndComp
Text GLabel 925  4025 0    39   Input ~ 0
REG
$Comp
L BC807 Q4
U 1 1 5A08982C
P 2275 4725
F 0 "Q4" H 2475 4800 50  0000 L CNN
F 1 "BC807" H 2475 4725 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 2475 4650 50  0001 L CIN
F 3 "" H 2275 4725 50  0001 L CNN
	1    2275 4725
	1    0    0    1   
$EndComp
$Comp
L R_Small 6R1
U 1 1 5A089869
P 2050 4025
F 0 "6R1" H 2080 4045 50  0000 L CNN
F 1 "6R2" H 2080 3985 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 2050 4025 50  0001 C CNN
F 3 "" H 2050 4025 50  0001 C CNN
	1    2050 4025
	0    -1   1    0   
$EndComp
$Comp
L R_Small R5
U 1 1 5A0899B9
P 2050 4300
F 0 "R5" H 2080 4320 50  0000 L CNN
F 1 "6R2" H 2080 4260 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 2050 4300 50  0001 C CNN
F 3 "" H 2050 4300 50  0001 C CNN
	1    2050 4300
	0    -1   1    0   
$EndComp
$Comp
L R_Small R6
U 1 1 5A089D3D
P 2800 5075
F 0 "R6" H 2830 5095 50  0000 L CNN
F 1 "100k" H 2830 5035 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 2800 5075 50  0001 C CNN
F 3 "" H 2800 5075 50  0001 C CNN
	1    2800 5075
	0    -1   1    0   
$EndComp
$Comp
L +3.3V #PWR012
U 1 1 5A08A07E
P 3100 3775
F 0 "#PWR012" H 3100 3625 50  0001 C CNN
F 1 "+3.3V" H 3100 3915 50  0000 C CNN
F 2 "" H 3100 3775 50  0001 C CNN
F 3 "" H 3100 3775 50  0001 C CNN
	1    3100 3775
	1    0    0    -1  
$EndComp
$Comp
L CP_Small C7
U 1 1 5A08AB40
P 3100 4600
F 0 "C7" H 3110 4670 50  0000 L CNN
F 1 "330uF" H 3110 4520 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-D_EIA-7343-31_Hand" H 3100 4600 50  0001 C CNN
F 3 "" H 3100 4600 50  0001 C CNN
	1    3100 4600
	1    0    0    -1  
$EndComp
$Comp
L D_Photo_ALT D2
U 1 1 5A08BF88
P 5925 1625
F 0 "D2" H 5945 1695 50  0000 L CNN
F 1 "FT" H 5885 1515 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 5875 1625 50  0001 C CNN
F 3 "" H 5875 1625 50  0001 C CNN
	1    5925 1625
	0    -1   -1   0   
$EndComp
$Comp
L +3.3V #PWR013
U 1 1 5A08C0B0
P 5925 1450
F 0 "#PWR013" H 5925 1300 50  0001 C CNN
F 1 "+3.3V" H 5925 1590 50  0000 C CNN
F 2 "" H 5925 1450 50  0001 C CNN
F 3 "" H 5925 1450 50  0001 C CNN
	1    5925 1450
	1    0    0    -1  
$EndComp
$Comp
L POT RV1
U 1 1 5A08C1AD
P 5925 2100
F 0 "RV1" V 5750 2100 50  0000 C CNN
F 1 "POT" V 5825 2100 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Trimmer_Vishay_TS53YJ" H 5925 2100 50  0001 C CNN
F 3 "" H 5925 2100 50  0001 C CNN
	1    5925 2100
	-1   0    0    1   
$EndComp
$Comp
L LED_ALT D1
U 1 1 5A08C345
P 5050 1450
F 0 "D1" H 5050 1550 50  0000 C CNN
F 1 "IR" H 5050 1350 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 5050 1450 50  0001 C CNN
F 3 "" H 5050 1450 50  0001 C CNN
	1    5050 1450
	0    -1   -1   0   
$EndComp
$Comp
L C_Small C2
U 1 1 5A08CAA7
P 6375 2100
F 0 "C2" H 6385 2170 50  0000 L CNN
F 1 "100n" H 6385 2020 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6375 2100 50  0001 C CNN
F 3 "" H 6375 2100 50  0001 C CNN
	1    6375 2100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 5A08CB5B
P 6375 2300
F 0 "#PWR014" H 6375 2050 50  0001 C CNN
F 1 "GND" H 6375 2150 50  0000 C CNN
F 2 "" H 6375 2300 50  0001 C CNN
F 3 "" H 6375 2300 50  0001 C CNN
	1    6375 2300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 5A08CBEC
P 5675 2300
F 0 "#PWR015" H 5675 2050 50  0001 C CNN
F 1 "GND" H 5675 2150 50  0000 C CNN
F 2 "" H 5675 2300 50  0001 C CNN
F 3 "" H 5675 2300 50  0001 C CNN
	1    5675 2300
	1    0    0    -1  
$EndComp
Text GLabel 6700 1900 2    39   Input ~ 0
ADC1
$Comp
L Q_NMOS_GSD Q2
U 1 1 5A08E1CB
P 4950 3625
F 0 "Q2" H 5150 3675 50  0000 L CNN
F 1 "Q_NMOS_GSD" H 5150 3575 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 5150 3725 50  0001 C CNN
F 3 "" H 4950 3625 50  0001 C CNN
	1    4950 3625
	1    0    0    -1  
$EndComp
$Comp
L R_Small R3
U 1 1 5A08E1D1
P 4650 3825
F 0 "R3" H 4680 3845 50  0000 L CNN
F 1 "100K" H 4680 3785 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 4650 3825 50  0001 C CNN
F 3 "" H 4650 3825 50  0001 C CNN
	1    4650 3825
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 5A08E1D7
P 4650 4025
F 0 "#PWR016" H 4650 3775 50  0001 C CNN
F 1 "GND" H 4650 3875 50  0000 C CNN
F 2 "" H 4650 4025 50  0001 C CNN
F 3 "" H 4650 4025 50  0001 C CNN
	1    4650 4025
	1    0    0    -1  
$EndComp
Text GLabel 4250 3625 0    39   Input ~ 0
PWM2
$Comp
L C_Small C5
U 1 1 5A08E1DE
P 4450 3625
F 0 "C5" H 4460 3695 50  0000 L CNN
F 1 "100n" H 4460 3545 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4450 3625 50  0001 C CNN
F 3 "" H 4450 3625 50  0001 C CNN
	1    4450 3625
	0    -1   -1   0   
$EndComp
Text GLabel 5050 2900 1    39   Input ~ 0
REG
$Comp
L GND #PWR017
U 1 1 5A08E1E5
P 5050 4025
F 0 "#PWR017" H 5050 3775 50  0001 C CNN
F 1 "GND" H 5050 3875 50  0000 C CNN
F 2 "" H 5050 4025 50  0001 C CNN
F 3 "" H 5050 4025 50  0001 C CNN
	1    5050 4025
	1    0    0    -1  
$EndComp
$Comp
L D_Photo_ALT D4
U 1 1 5A08E1EB
P 5925 3350
F 0 "D4" H 5945 3420 50  0000 L CNN
F 1 "FT" H 5885 3240 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 5875 3350 50  0001 C CNN
F 3 "" H 5875 3350 50  0001 C CNN
	1    5925 3350
	0    -1   -1   0   
$EndComp
$Comp
L +3.3V #PWR018
U 1 1 5A08E1F1
P 5925 3175
F 0 "#PWR018" H 5925 3025 50  0001 C CNN
F 1 "+3.3V" H 5925 3315 50  0000 C CNN
F 2 "" H 5925 3175 50  0001 C CNN
F 3 "" H 5925 3175 50  0001 C CNN
	1    5925 3175
	1    0    0    -1  
$EndComp
$Comp
L POT RV2
U 1 1 5A08E1F7
P 5925 3825
F 0 "RV2" V 5750 3825 50  0000 C CNN
F 1 "POT" V 5825 3825 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Trimmer_Vishay_TS53YJ" H 5925 3825 50  0001 C CNN
F 3 "" H 5925 3825 50  0001 C CNN
	1    5925 3825
	-1   0    0    -1  
$EndComp
$Comp
L LED_ALT D3
U 1 1 5A08E1FD
P 5050 3175
F 0 "D3" H 5050 3275 50  0000 C CNN
F 1 "IR" H 5050 3075 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 5050 3175 50  0001 C CNN
F 3 "" H 5050 3175 50  0001 C CNN
	1    5050 3175
	0    -1   -1   0   
$EndComp
$Comp
L C_Small C6
U 1 1 5A08E203
P 6375 3825
F 0 "C6" H 6385 3895 50  0000 L CNN
F 1 "100n" H 6385 3745 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6375 3825 50  0001 C CNN
F 3 "" H 6375 3825 50  0001 C CNN
	1    6375 3825
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR019
U 1 1 5A08E209
P 6375 4025
F 0 "#PWR019" H 6375 3775 50  0001 C CNN
F 1 "GND" H 6375 3875 50  0000 C CNN
F 2 "" H 6375 4025 50  0001 C CNN
F 3 "" H 6375 4025 50  0001 C CNN
	1    6375 4025
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR020
U 1 1 5A08E20F
P 5675 4025
F 0 "#PWR020" H 5675 3775 50  0001 C CNN
F 1 "GND" H 5675 3875 50  0000 C CNN
F 2 "" H 5675 4025 50  0001 C CNN
F 3 "" H 5675 4025 50  0001 C CNN
	1    5675 4025
	1    0    0    -1  
$EndComp
Text GLabel 6700 3625 2    39   Input ~ 0
ADC2
$Comp
L Q_NMOS_GSD Q5
U 1 1 5A08E7D2
P 4950 5350
F 0 "Q5" H 5150 5400 50  0000 L CNN
F 1 "Q_NMOS_GSD" H 5150 5300 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 5150 5450 50  0001 C CNN
F 3 "" H 4950 5350 50  0001 C CNN
	1    4950 5350
	1    0    0    -1  
$EndComp
$Comp
L R_Small R7
U 1 1 5A08E7D8
P 4650 5550
F 0 "R7" H 4680 5570 50  0000 L CNN
F 1 "100K" H 4680 5510 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 4650 5550 50  0001 C CNN
F 3 "" H 4650 5550 50  0001 C CNN
	1    4650 5550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR021
U 1 1 5A08E7DE
P 4650 5750
F 0 "#PWR021" H 4650 5500 50  0001 C CNN
F 1 "GND" H 4650 5600 50  0000 C CNN
F 2 "" H 4650 5750 50  0001 C CNN
F 3 "" H 4650 5750 50  0001 C CNN
	1    4650 5750
	1    0    0    -1  
$EndComp
Text GLabel 4250 5350 0    39   Input ~ 0
PWM3
$Comp
L C_Small C8
U 1 1 5A08E7E5
P 4450 5350
F 0 "C8" H 4460 5420 50  0000 L CNN
F 1 "100n" H 4460 5270 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4450 5350 50  0001 C CNN
F 3 "" H 4450 5350 50  0001 C CNN
	1    4450 5350
	0    -1   -1   0   
$EndComp
Text GLabel 5050 4625 1    39   Input ~ 0
REG
$Comp
L GND #PWR022
U 1 1 5A08E7EC
P 5050 5750
F 0 "#PWR022" H 5050 5500 50  0001 C CNN
F 1 "GND" H 5050 5600 50  0000 C CNN
F 2 "" H 5050 5750 50  0001 C CNN
F 3 "" H 5050 5750 50  0001 C CNN
	1    5050 5750
	1    0    0    -1  
$EndComp
$Comp
L D_Photo_ALT D6
U 1 1 5A08E7F2
P 5925 5075
F 0 "D6" H 5945 5145 50  0000 L CNN
F 1 "FT" H 5885 4965 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 5875 5075 50  0001 C CNN
F 3 "" H 5875 5075 50  0001 C CNN
	1    5925 5075
	0    -1   -1   0   
$EndComp
$Comp
L +3.3V #PWR023
U 1 1 5A08E7F8
P 5925 4900
F 0 "#PWR023" H 5925 4750 50  0001 C CNN
F 1 "+3.3V" H 5925 5040 50  0000 C CNN
F 2 "" H 5925 4900 50  0001 C CNN
F 3 "" H 5925 4900 50  0001 C CNN
	1    5925 4900
	1    0    0    -1  
$EndComp
$Comp
L POT RV3
U 1 1 5A08E7FE
P 5925 5550
F 0 "RV3" V 5750 5550 50  0000 C CNN
F 1 "POT" V 5825 5550 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Trimmer_Vishay_TS53YJ" H 5925 5550 50  0001 C CNN
F 3 "" H 5925 5550 50  0001 C CNN
	1    5925 5550
	-1   0    0    1   
$EndComp
$Comp
L LED_ALT D5
U 1 1 5A08E804
P 5050 4900
F 0 "D5" H 5050 5000 50  0000 C CNN
F 1 "IR" H 5050 4800 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 5050 4900 50  0001 C CNN
F 3 "" H 5050 4900 50  0001 C CNN
	1    5050 4900
	0    -1   -1   0   
$EndComp
$Comp
L C_Small C9
U 1 1 5A08E80A
P 6375 5550
F 0 "C9" H 6385 5620 50  0000 L CNN
F 1 "100n" H 6385 5470 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6375 5550 50  0001 C CNN
F 3 "" H 6375 5550 50  0001 C CNN
	1    6375 5550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR024
U 1 1 5A08E810
P 6375 5750
F 0 "#PWR024" H 6375 5500 50  0001 C CNN
F 1 "GND" H 6375 5600 50  0000 C CNN
F 2 "" H 6375 5750 50  0001 C CNN
F 3 "" H 6375 5750 50  0001 C CNN
	1    6375 5750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR025
U 1 1 5A08E816
P 5675 5750
F 0 "#PWR025" H 5675 5500 50  0001 C CNN
F 1 "GND" H 5675 5600 50  0000 C CNN
F 2 "" H 5675 5750 50  0001 C CNN
F 3 "" H 5675 5750 50  0001 C CNN
	1    5675 5750
	1    0    0    -1  
$EndComp
Text GLabel 6700 5350 2    39   Input ~ 0
ADC3
$Comp
L MCP6004 U1
U 1 1 5A0914AB
P 7750 2000
F 0 "U1" H 7750 2200 50  0000 L CNN
F 1 "MCP6021" H 7750 1800 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 7700 2100 50  0001 C CNN
F 3 "" H 7800 2200 50  0001 C CNN
	1    7750 2000
	1    0    0    -1  
$EndComp
Text GLabel 7350 1900 0    39   Input ~ 0
ADC1
$Comp
L +3.3V #PWR026
U 1 1 5A092981
P 7650 1650
F 0 "#PWR026" H 7650 1500 50  0001 C CNN
F 1 "+3.3V" H 7650 1790 50  0000 C CNN
F 2 "" H 7650 1650 50  0001 C CNN
F 3 "" H 7650 1650 50  0001 C CNN
	1    7650 1650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR027
U 1 1 5A092B05
P 7650 2475
F 0 "#PWR027" H 7650 2225 50  0001 C CNN
F 1 "GND" H 7650 2325 50  0000 C CNN
F 2 "" H 7650 2475 50  0001 C CNN
F 3 "" H 7650 2475 50  0001 C CNN
	1    7650 2475
	1    0    0    -1  
$EndComp
$Comp
L R_Small R2
U 1 1 5A092DEA
P 7925 2350
F 0 "R2" H 7955 2370 50  0000 L CNN
F 1 "0R" H 7955 2310 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 7925 2350 50  0001 C CNN
F 3 "" H 7925 2350 50  0001 C CNN
	1    7925 2350
	0    -1   1    0   
$EndComp
Text GLabel 8250 2000 2    39   Input ~ 0
xADC1
$Comp
L MCP6004 U1
U 3 1 5A093DA3
P 7750 3725
F 0 "U1" H 7750 3925 50  0000 L CNN
F 1 "MCP6021" H 7750 3525 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 7700 3825 50  0001 C CNN
F 3 "" H 7800 3925 50  0001 C CNN
	3    7750 3725
	1    0    0    -1  
$EndComp
Text GLabel 7350 3625 0    39   Input ~ 0
ADC2
$Comp
L +3.3V #PWR028
U 1 1 5A093DAB
P 7650 3375
F 0 "#PWR028" H 7650 3225 50  0001 C CNN
F 1 "+3.3V" H 7650 3515 50  0000 C CNN
F 2 "" H 7650 3375 50  0001 C CNN
F 3 "" H 7650 3375 50  0001 C CNN
	1    7650 3375
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR029
U 1 1 5A093DB2
P 7650 4200
F 0 "#PWR029" H 7650 3950 50  0001 C CNN
F 1 "GND" H 7650 4050 50  0000 C CNN
F 2 "" H 7650 4200 50  0001 C CNN
F 3 "" H 7650 4200 50  0001 C CNN
	1    7650 4200
	1    0    0    -1  
$EndComp
$Comp
L R_Small R4
U 1 1 5A093DBC
P 7925 4075
F 0 "R4" H 7955 4095 50  0000 L CNN
F 1 "0R" H 7955 4035 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 7925 4075 50  0001 C CNN
F 3 "" H 7925 4075 50  0001 C CNN
	1    7925 4075
	0    -1   1    0   
$EndComp
Text GLabel 8250 3725 2    39   Input ~ 0
xADC2
$Comp
L MCP6004 U1
U 4 1 5A094405
P 7750 5450
F 0 "U1" H 7750 5650 50  0000 L CNN
F 1 "MCP6021" H 7750 5250 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 7700 5550 50  0001 C CNN
F 3 "" H 7800 5650 50  0001 C CNN
	4    7750 5450
	1    0    0    -1  
$EndComp
Text GLabel 7350 5350 0    39   Input ~ 0
ADC3
$Comp
L +3.3V #PWR030
U 1 1 5A09440D
P 7650 5100
F 0 "#PWR030" H 7650 4950 50  0001 C CNN
F 1 "+3.3V" H 7650 5240 50  0000 C CNN
F 2 "" H 7650 5100 50  0001 C CNN
F 3 "" H 7650 5100 50  0001 C CNN
	1    7650 5100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR031
U 1 1 5A094414
P 7650 5925
F 0 "#PWR031" H 7650 5675 50  0001 C CNN
F 1 "GND" H 7650 5775 50  0000 C CNN
F 2 "" H 7650 5925 50  0001 C CNN
F 3 "" H 7650 5925 50  0001 C CNN
	1    7650 5925
	1    0    0    -1  
$EndComp
$Comp
L R_Small R8
U 1 1 5A09441E
P 7925 5800
F 0 "R8" H 7955 5820 50  0000 L CNN
F 1 "0R" H 7955 5760 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 7925 5800 50  0001 C CNN
F 3 "" H 7925 5800 50  0001 C CNN
	1    7925 5800
	0    -1   1    0   
$EndComp
Text GLabel 8250 5450 2    39   Input ~ 0
xADC3
$Comp
L Conn_01x08 J1
U 1 1 5A0953C0
P 9875 3050
F 0 "J1" H 9875 3450 50  0000 C CNN
F 1 "FTIR" H 9875 2550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08_Pitch2.54mm" H 9875 3050 50  0001 C CNN
F 3 "" H 9875 3050 50  0001 C CNN
	1    9875 3050
	-1   0    0    -1  
$EndComp
Text GLabel 10175 2950 2    39   Input ~ 0
xADC1
Text GLabel 10175 3050 2    39   Input ~ 0
xADC2
Text GLabel 10175 3150 2    39   Input ~ 0
xADC3
Text GLabel 10175 3250 2    39   Input ~ 0
PWM1
Text GLabel 10175 3350 2    39   Input ~ 0
PWM2
Text GLabel 10175 3450 2    39   Input ~ 0
PWM3
NoConn ~ 5925 2250
NoConn ~ 5925 5700
NoConn ~ 5925 3975
Text GLabel 9900 4700 2    60   Input ~ 0
5V
Text GLabel 10175 2850 2    39   Input ~ 0
GND
Text GLabel 10175 2750 2    39   Input ~ 0
5V
Text GLabel 10400 4700 2    60   Input ~ 0
GND
Wire Wire Line
	9800 4600 9800 4800
Wire Wire Line
	10300 4600 10300 4800
Connection ~ 10300 4700
Wire Wire Line
	1875 2150 1675 2150
Wire Wire Line
	1675 1950 1675 2275
Wire Wire Line
	2700 1950 2700 2275
Wire Wire Line
	2700 2150 2475 2150
Connection ~ 1675 2150
Connection ~ 2700 2150
Wire Wire Line
	1675 2575 1675 2475
Wire Wire Line
	2700 2575 2700 2475
Wire Wire Line
	2175 2575 2175 2450
Wire Wire Line
	5050 1600 5050 1700
Wire Wire Line
	4650 2000 4650 1900
Connection ~ 4650 1900
Wire Wire Line
	4650 2300 4650 2200
Wire Wire Line
	4350 1900 4250 1900
Wire Wire Line
	4550 1900 4750 1900
Wire Wire Line
	5050 2300 5050 2100
Wire Wire Line
	925  4025 1100 4025
Wire Wire Line
	1500 4025 1950 4025
Wire Wire Line
	1950 4300 1725 4300
Connection ~ 1725 4025
Wire Wire Line
	2150 4300 2375 4300
Wire Wire Line
	2375 4025 2375 4525
Connection ~ 2375 4300
Wire Wire Line
	1725 4725 2075 4725
Connection ~ 1725 4300
Wire Wire Line
	2375 4925 2375 5075
Wire Wire Line
	1300 5075 2700 5075
Wire Wire Line
	1300 5075 1300 4325
Connection ~ 2375 5075
Wire Wire Line
	2900 5075 3100 5075
Wire Wire Line
	3100 4700 3100 5200
Wire Wire Line
	3100 3775 3100 4500
Connection ~ 2375 4025
Connection ~ 3100 4025
Wire Wire Line
	5925 1450 5925 1525
Wire Wire Line
	5925 1825 5925 1950
Wire Wire Line
	5050 1175 5050 1300
Wire Wire Line
	5925 1900 6700 1900
Connection ~ 5925 1900
Wire Wire Line
	6375 1900 6375 2000
Wire Wire Line
	6375 2300 6375 2200
Connection ~ 6375 1900
Wire Wire Line
	5050 3325 5050 3425
Wire Wire Line
	4650 3725 4650 3625
Connection ~ 4650 3625
Wire Wire Line
	4650 4025 4650 3925
Wire Wire Line
	4350 3625 4250 3625
Wire Wire Line
	4550 3625 4750 3625
Wire Wire Line
	5050 4025 5050 3825
Wire Wire Line
	5925 3175 5925 3250
Wire Wire Line
	5925 3550 5925 3675
Wire Wire Line
	5050 2900 5050 3025
Wire Wire Line
	6375 3625 6375 3725
Wire Wire Line
	6375 4025 6375 3925
Connection ~ 6375 3625
Wire Wire Line
	5050 5050 5050 5150
Wire Wire Line
	4650 5450 4650 5350
Connection ~ 4650 5350
Wire Wire Line
	4650 5750 4650 5650
Wire Wire Line
	4350 5350 4250 5350
Wire Wire Line
	4550 5350 4750 5350
Wire Wire Line
	5050 5750 5050 5550
Wire Wire Line
	5925 4900 5925 4975
Wire Wire Line
	5925 5275 5925 5400
Wire Wire Line
	5050 4625 5050 4750
Wire Wire Line
	5925 5350 6700 5350
Connection ~ 5925 5350
Wire Wire Line
	6375 5350 6375 5450
Wire Wire Line
	6375 5750 6375 5650
Connection ~ 6375 5350
Wire Wire Line
	7450 1900 7350 1900
Wire Wire Line
	7650 1650 7650 1700
Wire Wire Line
	7650 2300 7650 2475
Wire Wire Line
	7450 2100 7350 2100
Wire Wire Line
	7350 2100 7350 2350
Wire Wire Line
	8150 2000 8150 2350
Wire Wire Line
	7350 2350 7825 2350
Wire Wire Line
	8150 2350 8025 2350
Wire Wire Line
	8050 2000 8250 2000
Connection ~ 8150 2000
Wire Wire Line
	7450 3625 7350 3625
Wire Wire Line
	7650 3375 7650 3425
Wire Wire Line
	7650 4025 7650 4200
Wire Wire Line
	7450 3825 7350 3825
Wire Wire Line
	7350 3825 7350 4075
Wire Wire Line
	8150 3725 8150 4075
Wire Wire Line
	7350 4075 7825 4075
Wire Wire Line
	8150 4075 8025 4075
Wire Wire Line
	8050 3725 8250 3725
Connection ~ 8150 3725
Wire Wire Line
	7450 5350 7350 5350
Wire Wire Line
	7650 5100 7650 5150
Wire Wire Line
	7650 5750 7650 5925
Wire Wire Line
	7450 5550 7350 5550
Wire Wire Line
	7350 5550 7350 5800
Wire Wire Line
	8150 5450 8150 5800
Wire Wire Line
	7350 5800 7825 5800
Wire Wire Line
	8150 5800 8025 5800
Wire Wire Line
	8050 5450 8250 5450
Connection ~ 8150 5450
Wire Wire Line
	10075 2750 10175 2750
Wire Wire Line
	10175 2950 10075 2950
Wire Wire Line
	10075 3050 10175 3050
Wire Wire Line
	10175 3150 10075 3150
Wire Wire Line
	10075 3250 10175 3250
Wire Wire Line
	10175 3350 10075 3350
Wire Wire Line
	5775 5550 5675 5550
Wire Wire Line
	5675 5550 5675 5750
Wire Wire Line
	5675 4025 5675 3825
Wire Wire Line
	5675 3825 5775 3825
Wire Wire Line
	5675 2300 5675 2100
Wire Wire Line
	5675 2100 5775 2100
Wire Wire Line
	5925 3625 6700 3625
Connection ~ 5925 3625
Wire Wire Line
	10075 3450 10175 3450
Connection ~ 9800 4700
Wire Wire Line
	10400 4700 10300 4700
Wire Wire Line
	9900 4700 9800 4700
Wire Wire Line
	10175 2850 10075 2850
Wire Wire Line
	2150 4025 3100 4025
$Comp
L R_Small R9
U 1 1 5A0B345C
P 1725 4525
F 0 "R9" H 1755 4545 50  0000 L CNN
F 1 "R_Small" H 1755 4485 50  0000 L CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 1725 4525 50  0001 C CNN
F 3 "" H 1725 4525 50  0001 C CNN
	1    1725 4525
	1    0    0    -1  
$EndComp
Wire Wire Line
	1725 4025 1725 4425
Wire Wire Line
	1725 4625 1725 4725
$Comp
L GND #PWR?
U 1 1 5A0B48FC
P 3100 5200
F 0 "#PWR?" H 3100 4950 50  0001 C CNN
F 1 "GND" H 3100 5050 50  0000 C CNN
F 2 "" H 3100 5200 50  0001 C CNN
F 3 "" H 3100 5200 50  0001 C CNN
	1    3100 5200
	1    0    0    -1  
$EndComp
Connection ~ 3100 5075
$EndSCHEMATC
