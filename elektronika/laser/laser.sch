EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:laser-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Stanowisko laboratoryjne do testowania czujników odległości."
Date "2017-11-07"
Rev "1"
Comp "Politechnika Wrocławska."
Comment1 "Wydział Elektroniki, "
Comment2 "Katedra Cybernetyki i Robotyki,"
Comment3 "Piotr Krzysztof Portasiak, 218594,"
Comment4 ""
$EndDescr
$Comp
L PWR_FLAG #FLG01
U 1 1 5A01041A
P 7300 4300
F 0 "#FLG01" H 7300 4375 50  0001 C CNN
F 1 "PWR_FLAG" H 7300 4450 50  0000 C CNN
F 2 "" H 7300 4300 50  0001 C CNN
F 3 "" H 7300 4300 50  0001 C CNN
	1    7300 4300
	1    0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 5A010444
P 7800 4100
F 0 "#FLG02" H 7800 4175 50  0001 C CNN
F 1 "PWR_FLAG" H 7800 4250 50  0000 C CNN
F 2 "" H 7800 4100 50  0001 C CNN
F 3 "" H 7800 4100 50  0001 C CNN
	1    7800 4100
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5A010461
P 7800 4300
F 0 "#PWR03" H 7800 4050 50  0001 C CNN
F 1 "GND" H 7800 4150 50  0000 C CNN
F 2 "" H 7800 4300 50  0001 C CNN
F 3 "" H 7800 4300 50  0001 C CNN
	1    7800 4300
	-1   0    0    -1  
$EndComp
Text GLabel 7400 4200 2    60   Input ~ 0
5V
Text GLabel 7900 4200 2    60   Input ~ 0
GND
$Comp
L R_Small R2
U 1 1 5A0916FB
P 3825 4025
F 0 "R2" H 3855 4045 50  0000 L CNN
F 1 "1k" H 3855 3985 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3825 4025 50  0001 C CNN
F 3 "" H 3825 4025 50  0001 C CNN
	1    3825 4025
	0    1    1    0   
$EndComp
$Comp
L GND #PWR04
U 1 1 5A0916F5
P 4350 4425
F 0 "#PWR04" H 4350 4175 50  0001 C CNN
F 1 "GND" H 4350 4275 50  0000 C CNN
F 2 "" H 4350 4425 50  0001 C CNN
F 3 "" H 4350 4425 50  0001 C CNN
	1    4350 4425
	1    0    0    -1  
$EndComp
Text GLabel 3325 4025 0    39   Input ~ 0
LASER_ON
$Comp
L BC817 Q1
U 1 1 5A0A2ED9
P 4250 4025
F 0 "Q1" H 4450 4100 50  0000 L CNN
F 1 "BC817" H 4450 4025 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 4450 3950 50  0001 L CIN
F 3 "" H 4250 4025 50  0001 L CNN
	1    4250 4025
	1    0    0    -1  
$EndComp
$Comp
L R_Small R3
U 1 1 5A0A2FD7
P 3525 4225
F 0 "R3" H 3555 4245 50  0000 L CNN
F 1 "10K" H 3555 4185 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3525 4225 50  0001 C CNN
F 3 "" H 3525 4225 50  0001 C CNN
	1    3525 4225
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 5A0A30C1
P 3525 4425
F 0 "#PWR05" H 3525 4175 50  0001 C CNN
F 1 "GND" H 3525 4275 50  0000 C CNN
F 2 "" H 3525 4425 50  0001 C CNN
F 3 "" H 3525 4425 50  0001 C CNN
	1    3525 4425
	1    0    0    -1  
$EndComp
$Comp
L R_Small R1
U 1 1 5A0A31E3
P 4350 3625
F 0 "R1" H 4380 3645 50  0000 L CNN
F 1 "1k" H 4380 3585 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 4350 3625 50  0001 C CNN
F 3 "" H 4350 3625 50  0001 C CNN
	1    4350 3625
	1    0    0    -1  
$EndComp
Text GLabel 4350 3375 1    39   Input ~ 0
LASER_GND
$Comp
L +5V #PWR06
U 1 1 5A0A354E
P 7300 4100
F 0 "#PWR06" H 7300 3950 50  0001 C CNN
F 1 "+5V" H 7300 4240 50  0000 C CNN
F 2 "" H 7300 4100 50  0001 C CNN
F 3 "" H 7300 4100 50  0001 C CNN
	1    7300 4100
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x03 J1
U 1 1 5A0A3675
P 5500 4250
F 0 "J1" H 5500 4450 50  0000 C CNN
F 1 "Conn_01x03" H 5500 4050 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 5500 4250 50  0001 C CNN
F 3 "" H 5500 4250 50  0001 C CNN
	1    5500 4250
	-1   0    0    -1  
$EndComp
Text GLabel 5800 4350 2    39   Input ~ 0
LASER_ON
Wire Wire Line
	7300 4100 7300 4300
Wire Wire Line
	7800 4100 7800 4300
Wire Wire Line
	7400 4200 7300 4200
Connection ~ 7300 4200
Wire Wire Line
	7900 4200 7800 4200
Connection ~ 7800 4200
Wire Wire Line
	4350 4425 4350 4225
Wire Wire Line
	3925 4025 4050 4025
Wire Wire Line
	3325 4025 3725 4025
Wire Wire Line
	4350 3725 4350 3825
Wire Wire Line
	3525 4125 3525 4025
Connection ~ 3525 4025
Wire Wire Line
	3525 4425 3525 4325
Wire Wire Line
	4350 3375 4350 3525
Wire Wire Line
	5800 4350 5700 4350
Text GLabel 5800 4250 2    39   Input ~ 0
GND
Text GLabel 5800 4150 2    39   Input ~ 0
5V
Wire Wire Line
	5800 4150 5700 4150
Wire Wire Line
	5700 4250 5800 4250
$Comp
L Conn_01x02 J2
U 1 1 5A0A4269
P 5525 4800
F 0 "J2" H 5525 4900 50  0000 C CNN
F 1 "Conn_01x02" H 5525 4600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 5525 4800 50  0001 C CNN
F 3 "" H 5525 4800 50  0001 C CNN
	1    5525 4800
	-1   0    0    -1  
$EndComp
Text GLabel 5825 4800 2    39   Input ~ 0
5V
Wire Wire Line
	5725 4800 5825 4800
Text GLabel 5825 4900 2    39   Input ~ 0
LASER_GND
Wire Wire Line
	5825 4900 5725 4900
$EndSCHEMATC
