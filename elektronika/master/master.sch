EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:master-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Stanowisko laboratoryjne do testowania czujników odległości."
Date "2017-11-07"
Rev "1"
Comp "Politechnika Wrocławska."
Comment1 "Wydział Elektroniki, "
Comment2 "Katedra Cybernetyki i Robotyki,"
Comment3 "Piotr Krzysztof Portasiak, 218594,"
Comment4 ""
$EndDescr
$Comp
L LTV-847 U2
U 1 1 5A00F2BD
P 2050 5150
F 0 "U2" H 1850 5850 50  0000 L CNN
F 1 "LTV-847" H 2050 5850 50  0000 L CNN
F 2 "Housings_DIP:DIP-16_W8.89mm_SMDSocket_LongPads" H 1850 4550 50  0001 L CIN
F 3 "" H 2050 5050 50  0001 L CNN
	1    2050 5150
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J1
U 1 1 5A00F59F
P 1100 1300
F 0 "J1" H 1100 1400 50  0000 C CNN
F 1 "POWER" H 1100 1100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 1100 1300 50  0001 C CNN
F 3 "" H 1100 1300 50  0001 C CNN
	1    1100 1300
	-1   0    0    -1  
$EndComp
$Comp
L Q_PMOS_GSD Q1
U 1 1 5A00F73E
P 1700 1400
F 0 "Q1" H 1900 1450 50  0000 L CNN
F 1 "Q_PMOS_GSD" H 1900 1350 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 1900 1500 50  0001 C CNN
F 3 "" H 1700 1400 50  0001 C CNN
	1    1700 1400
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR01
U 1 1 5A00F83C
P 1400 1500
F 0 "#PWR01" H 1400 1250 50  0001 C CNN
F 1 "GND" H 1400 1350 50  0000 C CNN
F 2 "" H 1400 1500 50  0001 C CNN
F 3 "" H 1400 1500 50  0001 C CNN
	1    1400 1500
	1    0    0    -1  
$EndComp
$Comp
L R_Small R1
U 1 1 5A00F875
P 1700 1750
F 0 "R1" H 1730 1770 50  0000 L CNN
F 1 "100k" H 1730 1710 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 1700 1750 50  0001 C CNN
F 3 "" H 1700 1750 50  0001 C CNN
	1    1700 1750
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 5A01041A
P 9050 5650
F 0 "#FLG02" H 9050 5725 50  0001 C CNN
F 1 "PWR_FLAG" H 9050 5800 50  0000 C CNN
F 2 "" H 9050 5650 50  0001 C CNN
F 3 "" H 9050 5650 50  0001 C CNN
	1    9050 5650
	1    0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG03
U 1 1 5A010444
P 9550 5450
F 0 "#FLG03" H 9550 5525 50  0001 C CNN
F 1 "PWR_FLAG" H 9550 5600 50  0000 C CNN
F 2 "" H 9550 5450 50  0001 C CNN
F 3 "" H 9550 5450 50  0001 C CNN
	1    9550 5450
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5A010461
P 9550 5650
F 0 "#PWR04" H 9550 5400 50  0001 C CNN
F 1 "GND" H 9550 5500 50  0000 C CNN
F 2 "" H 9550 5650 50  0001 C CNN
F 3 "" H 9550 5650 50  0001 C CNN
	1    9550 5650
	-1   0    0    -1  
$EndComp
$Comp
L +5V #PWR05
U 1 1 5A010484
P 9050 5450
F 0 "#PWR05" H 9050 5300 50  0001 C CNN
F 1 "+5V" H 9050 5590 50  0000 C CNN
F 2 "" H 9050 5450 50  0001 C CNN
F 3 "" H 9050 5450 50  0001 C CNN
	1    9050 5450
	-1   0    0    -1  
$EndComp
$Comp
L C_Small C1
U 1 1 5A01063C
P 2250 1550
F 0 "C1" H 2260 1620 50  0000 L CNN
F 1 "100nF" H 2260 1470 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2250 1550 50  0001 C CNN
F 3 "" H 2250 1550 50  0001 C CNN
	1    2250 1550
	1    0    0    -1  
$EndComp
$Comp
L CP1_Small C3
U 1 1 5A01065F
P 2700 1550
F 0 "C3" H 2710 1620 50  0000 L CNN
F 1 "100uF" H 2710 1470 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-B_EIA-3528-21_Hand" H 2700 1550 50  0001 C CNN
F 3 "" H 2700 1550 50  0001 C CNN
	1    2700 1550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 5A0112A4
P 2700 2000
F 0 "#PWR06" H 2700 1750 50  0001 C CNN
F 1 "GND" H 2700 1850 50  0000 C CNN
F 2 "" H 2700 2000 50  0001 C CNN
F 3 "" H 2700 2000 50  0001 C CNN
	1    2700 2000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 5A00FA17
P 1700 2000
F 0 "#PWR07" H 1700 1750 50  0001 C CNN
F 1 "GND" H 1700 1850 50  0000 C CNN
F 2 "" H 1700 2000 50  0001 C CNN
F 3 "" H 1700 2000 50  0001 C CNN
	1    1700 2000
	1    0    0    -1  
$EndComp
Text GLabel 9150 5550 2    60   Input ~ 0
5V
Text GLabel 9650 5550 2    60   Input ~ 0
GND
Text GLabel 1650 4550 0    39   Input ~ 0
5V
Text GLabel 1650 4850 0    39   Input ~ 0
5V
Text GLabel 1650 5150 0    39   Input ~ 0
5V
Text GLabel 1650 5450 0    39   Input ~ 0
5V
$Comp
L R_Small R2
U 1 1 5A018104
P 2650 2800
F 0 "R2" H 2680 2820 50  0000 L CNN
F 1 "200R" H 2680 2760 50  0000 L CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 2650 2800 50  0001 C CNN
F 3 "" H 2650 2800 50  0001 C CNN
	1    2650 2800
	0    -1   -1   0   
$EndComp
$Comp
L R_Small R3
U 1 1 5A0182DA
P 2650 2900
F 0 "R3" H 2680 2920 50  0001 L CNN
F 1 "200R" H 2680 2860 50  0001 L CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 2650 2900 50  0001 C CNN
F 3 "" H 2650 2900 50  0001 C CNN
	1    2650 2900
	0    -1   -1   0   
$EndComp
$Comp
L R_Small R4
U 1 1 5A018311
P 2650 3000
F 0 "R4" H 2680 3020 50  0001 L CNN
F 1 "200R" H 2680 2960 50  0001 L CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 2650 3000 50  0001 C CNN
F 3 "" H 2650 3000 50  0001 C CNN
	1    2650 3000
	0    -1   -1   0   
$EndComp
$Comp
L R_Small R5
U 1 1 5A018349
P 2650 3100
F 0 "R5" H 2680 3120 50  0001 L CNN
F 1 "200R" H 2680 3060 50  0001 L CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 2650 3100 50  0001 C CNN
F 3 "" H 2650 3100 50  0001 C CNN
	1    2650 3100
	0    -1   -1   0   
$EndComp
Text GLabel 1650 5350 0    39   Input ~ 0
Q1
Text GLabel 2850 3100 2    39   Input ~ 0
Q4
Text GLabel 2850 3000 2    39   Input ~ 0
Q3
Text GLabel 2850 2900 2    39   Input ~ 0
Q2
Text GLabel 2850 2800 2    39   Input ~ 0
Q1
Text GLabel 1650 5650 0    39   Input ~ 0
Q2
Text GLabel 1650 5050 0    39   Input ~ 0
Q3
Text GLabel 1650 4750 0    39   Input ~ 0
Q4
NoConn ~ 2450 3600
Text GLabel 1550 2900 0    39   Input ~ 0
EN
Text GLabel 1550 2800 0    39   Input ~ 0
CLK
Text GLabel 1550 3000 0    39   Input ~ 0
DIR
Text GLabel 1550 3100 0    39   Input ~ 0
DIV
$Comp
L GND #PWR08
U 1 1 5A019ADB
P 2050 3850
F 0 "#PWR08" H 2050 3600 50  0001 C CNN
F 1 "GND" H 2050 3700 50  0000 C CNN
F 2 "" H 2050 3850 50  0001 C CNN
F 3 "" H 2050 3850 50  0001 C CNN
	1    2050 3850
	1    0    0    -1  
$EndComp
Text GLabel 2450 5450 2    39   Input ~ 0
xEN
Text GLabel 2450 5150 2    39   Input ~ 0
xCLK
Text GLabel 2450 4850 2    39   Input ~ 0
xDIR
Text GLabel 2450 4550 2    39   Input ~ 0
xM2
Text GLabel 2450 5350 2    39   Input ~ 0
xGND
Text GLabel 2450 5650 2    39   Input ~ 0
xGND
Text GLabel 2450 4750 2    39   Input ~ 0
xM1
Text GLabel 2450 5050 2    39   Input ~ 0
xGND
$Comp
L C_Small C6
U 1 1 5A01AA91
P 10200 2250
F 0 "C6" H 10210 2320 50  0000 L CNN
F 1 "100nF" H 10210 2170 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 10200 2250 50  0001 C CNN
F 3 "" H 10200 2250 50  0001 C CNN
	1    10200 2250
	1    0    0    -1  
$EndComp
$Comp
L C_Small C5
U 1 1 5A01AD19
P 8000 2250
F 0 "C5" H 8010 2320 50  0000 L CNN
F 1 "100nF" H 8010 2170 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 8000 2250 50  0001 C CNN
F 3 "" H 8000 2250 50  0001 C CNN
	1    8000 2250
	-1   0    0    -1  
$EndComp
$Comp
L C_Small C7
U 1 1 5A01B02B
P 10200 2600
F 0 "C7" H 10210 2670 50  0000 L CNN
F 1 "100nF" H 10210 2520 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 10200 2600 50  0001 C CNN
F 3 "" H 10200 2600 50  0001 C CNN
	1    10200 2600
	0    -1   1    0   
$EndComp
$Comp
L C_Small C4
U 1 1 5A01B4BE
P 8000 1700
F 0 "C4" H 8010 1770 50  0000 L CNN
F 1 "100nF" H 8010 1620 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 8000 1700 50  0001 C CNN
F 3 "" H 8000 1700 50  0001 C CNN
	1    8000 1700
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 5A01BAC4
P 8000 1850
F 0 "#PWR09" H 8000 1600 50  0001 C CNN
F 1 "GND" H 8000 1700 50  0000 C CNN
F 2 "" H 8000 1850 50  0001 C CNN
F 3 "" H 8000 1850 50  0001 C CNN
	1    8000 1850
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR010
U 1 1 5A01BB75
P 8000 1500
F 0 "#PWR010" H 8000 1350 50  0001 C CNN
F 1 "+3.3V" H 8000 1640 50  0000 C CNN
F 2 "" H 8000 1500 50  0001 C CNN
F 3 "" H 8000 1500 50  0001 C CNN
	1    8000 1500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 5A01D086
P 10500 3000
F 0 "#PWR011" H 10500 2750 50  0001 C CNN
F 1 "GND" H 10500 2850 50  0000 C CNN
F 2 "" H 10500 3000 50  0001 C CNN
F 3 "" H 10500 3000 50  0001 C CNN
	1    10500 3000
	1    0    0    -1  
$EndComp
$Comp
L C_Small C8
U 1 1 5A01B279
P 10200 2900
F 0 "C8" H 10210 2970 50  0000 L CNN
F 1 "100nF" H 10210 2820 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 10200 2900 50  0001 C CNN
F 3 "" H 10200 2900 50  0001 C CNN
	1    10200 2900
	0    -1   1    0   
$EndComp
Text GLabel 8000 3300 0    39   Input ~ 0
mTX
Text GLabel 8000 3700 0    39   Input ~ 0
mRX
Text GLabel 10000 3300 2    39   Input ~ 0
TX
Text GLabel 10000 3700 2    39   Input ~ 0
RX
$Comp
L GND #PWR012
U 1 1 5A020AF9
P 9100 4300
F 0 "#PWR012" H 9100 4050 50  0001 C CNN
F 1 "GND" H 9100 4150 50  0000 C CNN
F 2 "" H 9100 4300 50  0001 C CNN
F 3 "" H 9100 4300 50  0001 C CNN
	1    9100 4300
	1    0    0    -1  
$EndComp
NoConn ~ 4350 3100
NoConn ~ 4850 3100
NoConn ~ 4850 3200
NoConn ~ 4350 3200
NoConn ~ 4350 3300
NoConn ~ 4350 3400
NoConn ~ 4850 3500
NoConn ~ 4350 3500
NoConn ~ 4350 3600
NoConn ~ 4350 3700
NoConn ~ 4350 3800
NoConn ~ 4350 3900
NoConn ~ 4350 4000
NoConn ~ 4350 4100
NoConn ~ 4350 4200
NoConn ~ 4350 4300
NoConn ~ 4350 4400
NoConn ~ 4350 4500
NoConn ~ 4350 4600
NoConn ~ 4350 4700
NoConn ~ 4850 3600
NoConn ~ 4850 4200
NoConn ~ 4850 4300
Text GLabel 4950 3400 2    39   Input ~ 0
GND
Text GLabel 4950 3300 2    39   Input ~ 0
E5V
Text GLabel 4950 3800 2    39   Input ~ 0
3V3
$Comp
L PWR_FLAG #FLG013
U 1 1 5A023772
P 8550 5650
F 0 "#FLG013" H 8550 5725 50  0001 C CNN
F 1 "PWR_FLAG" H 8550 5800 50  0000 C CNN
F 2 "" H 8550 5650 50  0001 C CNN
F 3 "" H 8550 5650 50  0001 C CNN
	1    8550 5650
	1    0    0    1   
$EndComp
Text GLabel 8650 5550 2    60   Input ~ 0
3V3
$Comp
L +3.3V #PWR014
U 1 1 5A0238D5
P 8550 5450
F 0 "#PWR014" H 8550 5300 50  0001 C CNN
F 1 "+3.3V" H 8550 5590 50  0000 C CNN
F 2 "" H 8550 5450 50  0001 C CNN
F 3 "" H 8550 5450 50  0001 C CNN
	1    8550 5450
	1    0    0    -1  
$EndComp
Text GLabel 4950 3900 2    39   Input ~ 0
5V
Text GLabel 2900 1300 2    60   Input ~ 0
E5V
NoConn ~ 4850 3700
Text GLabel 4950 4000 2    39   Input ~ 0
GND
Text GLabel 4950 4100 2    39   Input ~ 0
GND
$Comp
L Conn_01x06 J6
U 1 1 5A0278FC
P 4500 1550
F 0 "J6" H 4500 1850 50  0000 C CNN
F 1 "MOTOR" H 4500 1150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 4500 1550 50  0001 C CNN
F 3 "" H 4500 1550 50  0001 C CNN
	1    4500 1550
	-1   0    0    -1  
$EndComp
Text GLabel 4800 1350 2    39   Input ~ 0
xEN
Text GLabel 4800 1450 2    39   Input ~ 0
xCLK
Text GLabel 4800 1650 2    39   Input ~ 0
xDIR
Text GLabel 4800 1550 2    39   Input ~ 0
xGND
Text GLabel 4800 1750 2    39   Input ~ 0
xM1
Text GLabel 4800 1850 2    39   Input ~ 0
xM2
NoConn ~ 6700 3100
NoConn ~ 6700 3200
NoConn ~ 6700 3500
NoConn ~ 6200 4000
NoConn ~ 6200 4200
NoConn ~ 6200 4300
NoConn ~ 6700 3600
NoConn ~ 6700 4200
NoConn ~ 6700 4300
NoConn ~ 6700 4400
NoConn ~ 6700 4500
NoConn ~ 6700 4600
NoConn ~ 6700 4700
NoConn ~ 6700 3700
Text GLabel 6100 4800 0    39   Input ~ 0
uRX
Text GLabel 6100 4900 0    39   Input ~ 0
uTX
NoConn ~ 6700 4800
NoConn ~ 6700 4900
NoConn ~ 6700 3300
NoConn ~ 6700 3400
NoConn ~ 6700 3800
NoConn ~ 6700 3900
NoConn ~ 6700 4000
NoConn ~ 6700 4100
$Comp
L Conn_01x04 J9
U 1 1 5A02E516
P 6150 1550
F 0 "J9" H 6150 1750 50  0000 C CNN
F 1 "USER" H 6150 1250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 6150 1550 50  0001 C CNN
F 3 "" H 6150 1550 50  0001 C CNN
	1    6150 1550
	-1   0    0    -1  
$EndComp
Text GLabel 6450 1450 2    39   Input ~ 0
3V3
Text GLabel 6450 1550 2    39   Input ~ 0
GND
Text GLabel 6450 1650 2    39   Input ~ 0
uRX
Text GLabel 6450 1750 2    39   Input ~ 0
uTX
Text GLabel 5650 1650 2    39   Input ~ 0
RX
Text GLabel 5650 1550 2    39   Input ~ 0
TX
$Comp
L Conn_01x06 J8
U 1 1 5A030FB1
P 5350 1550
F 0 "J8" H 5350 1850 50  0000 C CNN
F 1 "SLAVE" H 5350 1150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 5350 1550 50  0001 C CNN
F 3 "" H 5350 1550 50  0001 C CNN
	1    5350 1550
	-1   0    0    -1  
$EndComp
Text GLabel 5650 1850 2    39   Input ~ 0
D1
Text GLabel 5650 1750 2    39   Input ~ 0
D0
Text GLabel 6100 4100 0    39   Input ~ 0
mTX
Text GLabel 6100 4700 0    39   Input ~ 0
mRX
Text GLabel 4950 4600 2    39   Input ~ 0
DIR
Text GLabel 4950 4700 2    39   Input ~ 0
DIV
Text GLabel 4950 4500 2    39   Input ~ 0
EN
Text GLabel 4950 4400 2    39   Input ~ 0
CLK
$Comp
L Conn_02x19_Odd_Even J7
U 1 1 5A021397
P 4550 4000
F 0 "J7" H 4600 5000 50  0000 C CNN
F 1 "CN7" H 4600 3000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x19_Pitch2.54mm" H 4550 4000 50  0001 C CNN
F 3 "" H 4550 4000 50  0001 C CNN
	1    4550 4000
	1    0    0    -1  
$EndComp
Text GLabel 4950 4900 2    39   Input ~ 0
LED4
Text GLabel 4950 4800 2    39   Input ~ 0
LED2
Text GLabel 6100 4400 0    39   Input ~ 0
D0
Text GLabel 6100 4500 0    39   Input ~ 0
D1
$Comp
L Conn_02x19_Odd_Even J10
U 1 1 5A0288AB
P 6400 4000
F 0 "J10" H 6450 5000 50  0000 C CNN
F 1 "CN10" H 6450 3000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x19_Pitch2.54mm" H 6400 4000 50  0001 C CNN
F 3 "" H 6400 4000 50  0001 C CNN
	1    6400 4000
	1    0    0    -1  
$EndComp
$Comp
L ULN2803A U1
U 1 1 5A031CF6
P 2050 3100
F 0 "U1" H 2050 3625 50  0000 C CNN
F 1 "ULN2803A" H 2050 3550 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-18W_7.5x11.6mm_Pitch1.27mm" H 2100 2450 50  0001 L CNN
F 3 "" H 2150 3000 50  0001 C CNN
	1    2050 3100
	1    0    0    -1  
$EndComp
$Comp
L R_Small R7
U 1 1 5A0322FA
P 2650 3300
F 0 "R7" H 2680 3320 50  0001 L CNN
F 1 "200R" H 2680 3260 50  0001 L CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 2650 3300 50  0001 C CNN
F 3 "" H 2650 3300 50  0001 C CNN
	1    2650 3300
	0    -1   -1   0   
$EndComp
Text GLabel 2850 3300 2    39   Input ~ 0
Q6
$Comp
L R_Small R6
U 1 1 5A03235A
P 2650 3200
F 0 "R6" H 2680 3220 50  0001 L CNN
F 1 "200R" H 2680 3160 50  0001 L CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 2650 3200 50  0001 C CNN
F 3 "" H 2650 3200 50  0001 C CNN
	1    2650 3200
	0    -1   -1   0   
$EndComp
Text GLabel 2850 3200 2    39   Input ~ 0
Q5
Text GLabel 1550 3200 0    39   Input ~ 0
LED1
Text GLabel 1550 3300 0    39   Input ~ 0
LED2
NoConn ~ 9900 3500
NoConn ~ 9900 3100
$Comp
L MAX3232 U3
U 1 1 5A00EA1A
P 9100 3000
F 0 "U3" H 9000 4125 50  0000 R CNN
F 1 "MAX3232" H 9000 4050 50  0000 R CNN
F 2 "SMD_Packages:SO-16-N" H 9150 1950 50  0001 L CNN
F 3 "" H 9100 3100 50  0001 C CNN
	1    9100 3000
	1    0    0    -1  
$EndComp
NoConn ~ 8300 3500
NoConn ~ 8300 3100
NoConn ~ 6200 3100
NoConn ~ 6200 3400
NoConn ~ 6200 3500
NoConn ~ 6200 3600
NoConn ~ 6200 3700
NoConn ~ 6200 3800
NoConn ~ 6200 3900
Text GLabel 4350 4800 0    39   Input ~ 0
LED1
Text GLabel 4350 4900 0    39   Input ~ 0
LED3
$Comp
L R_Small R9
U 1 1 5A03F788
P 2650 3500
F 0 "R9" H 2680 3520 50  0001 L CNN
F 1 "200R" H 2680 3460 50  0001 L CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 2650 3500 50  0001 C CNN
F 3 "" H 2650 3500 50  0001 C CNN
	1    2650 3500
	0    -1   -1   0   
$EndComp
Text GLabel 2850 3500 2    39   Input ~ 0
Q8
$Comp
L R_Small R8
U 1 1 5A03F78F
P 2650 3400
F 0 "R8" H 2680 3420 50  0001 L CNN
F 1 "200R" H 2680 3360 50  0001 L CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 2650 3400 50  0001 C CNN
F 3 "" H 2650 3400 50  0001 C CNN
	1    2650 3400
	0    -1   -1   0   
$EndComp
Text GLabel 2850 3400 2    39   Input ~ 0
Q7
Text GLabel 1550 3400 0    39   Input ~ 0
LED3
Text GLabel 1550 3500 0    39   Input ~ 0
LED4
Text GLabel 5650 1450 2    39   Input ~ 0
GND
Text GLabel 5650 1350 2    39   Input ~ 0
x5V
Text GLabel 1400 1100 1    60   Input ~ 0
x5V
NoConn ~ 6200 4600
$Comp
L Conn_01x03 J11
U 1 1 5A048F45
P 5700 5900
F 0 "J11" H 5700 6100 50  0000 C CNN
F 1 "ENDL" H 5700 5700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 5700 5900 50  0001 C CNN
F 3 "" H 5700 5900 50  0001 C CNN
	1    5700 5900
	-1   0    0    -1  
$EndComp
Text GLabel 6000 6000 2    39   Input ~ 0
xENDL
$Comp
L C_Small C2
U 1 1 5A05087F
P 4700 5850
F 0 "C2" H 4710 5920 50  0000 L CNN
F 1 "100nF" H 4710 5770 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4700 5850 50  0001 C CNN
F 3 "" H 4700 5850 50  0001 C CNN
	1    4700 5850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 5A050885
P 4700 6050
F 0 "#PWR015" H 4700 5800 50  0001 C CNN
F 1 "GND" H 4700 5900 50  0000 C CNN
F 2 "" H 4700 6050 50  0001 C CNN
F 3 "" H 4700 6050 50  0001 C CNN
	1    4700 6050
	1    0    0    -1  
$EndComp
$Comp
L R_Small R10
U 1 1 5A05088B
P 4050 5650
F 0 "R10" H 4080 5670 50  0000 L CNN
F 1 "10k" H 4080 5610 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 4050 5650 50  0001 C CNN
F 3 "" H 4050 5650 50  0001 C CNN
	1    4050 5650
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR016
U 1 1 5A050891
P 4300 6050
F 0 "#PWR016" H 4300 5800 50  0001 C CNN
F 1 "GND" H 4300 5900 50  0000 C CNN
F 2 "" H 4300 6050 50  0001 C CNN
F 3 "" H 4300 6050 50  0001 C CNN
	1    4300 6050
	1    0    0    -1  
$EndComp
$Comp
L R_Small R11
U 1 1 5A050897
P 4300 5850
F 0 "R11" H 4330 5870 50  0000 L CNN
F 1 "20k" H 4330 5810 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 4300 5850 50  0001 C CNN
F 3 "" H 4300 5850 50  0001 C CNN
	1    4300 5850
	1    0    0    -1  
$EndComp
Text GLabel 3850 5650 0    39   Input ~ 0
xENDL
Text GLabel 4900 5650 2    39   Input ~ 0
ENDL
$Comp
L R_Small R12
U 1 1 5A0508AA
P 4500 5650
F 0 "R12" H 4530 5670 50  0000 L CNN
F 1 "1k" H 4530 5610 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 4500 5650 50  0001 C CNN
F 3 "" H 4500 5650 50  0001 C CNN
	1    4500 5650
	0    -1   -1   0   
$EndComp
Text GLabel 6000 5800 2    39   Input ~ 0
5V
Text GLabel 6000 5900 2    39   Input ~ 0
GND
$Comp
L Conn_01x03 J12
U 1 1 5A053730
P 5700 6950
F 0 "J12" H 5700 7150 50  0000 C CNN
F 1 "ENDP" H 5700 6750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 5700 6950 50  0001 C CNN
F 3 "" H 5700 6950 50  0001 C CNN
	1    5700 6950
	-1   0    0    -1  
$EndComp
Text GLabel 6000 7050 2    39   Input ~ 0
xENDP
$Comp
L C_Small C9
U 1 1 5A053737
P 4700 6900
F 0 "C9" H 4710 6970 50  0000 L CNN
F 1 "100nF" H 4710 6820 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4700 6900 50  0001 C CNN
F 3 "" H 4700 6900 50  0001 C CNN
	1    4700 6900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR017
U 1 1 5A05373D
P 4700 7100
F 0 "#PWR017" H 4700 6850 50  0001 C CNN
F 1 "GND" H 4700 6950 50  0000 C CNN
F 2 "" H 4700 7100 50  0001 C CNN
F 3 "" H 4700 7100 50  0001 C CNN
	1    4700 7100
	1    0    0    -1  
$EndComp
$Comp
L R_Small R13
U 1 1 5A053743
P 4050 6700
F 0 "R13" H 4080 6720 50  0000 L CNN
F 1 "10k" H 4080 6660 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 4050 6700 50  0001 C CNN
F 3 "" H 4050 6700 50  0001 C CNN
	1    4050 6700
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR018
U 1 1 5A053749
P 4300 7100
F 0 "#PWR018" H 4300 6850 50  0001 C CNN
F 1 "GND" H 4300 6950 50  0000 C CNN
F 2 "" H 4300 7100 50  0001 C CNN
F 3 "" H 4300 7100 50  0001 C CNN
	1    4300 7100
	1    0    0    -1  
$EndComp
$Comp
L R_Small R14
U 1 1 5A05374F
P 4300 6900
F 0 "R14" H 4330 6920 50  0000 L CNN
F 1 "20k" H 4330 6860 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 4300 6900 50  0001 C CNN
F 3 "" H 4300 6900 50  0001 C CNN
	1    4300 6900
	1    0    0    -1  
$EndComp
Text GLabel 3850 6700 0    39   Input ~ 0
xENDP
Text GLabel 4900 6700 2    39   Input ~ 0
ENDP
$Comp
L R_Small R15
U 1 1 5A053757
P 4500 6700
F 0 "R15" H 4530 6720 50  0000 L CNN
F 1 "1k" H 4530 6660 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 4500 6700 50  0001 C CNN
F 3 "" H 4500 6700 50  0001 C CNN
	1    4500 6700
	0    -1   -1   0   
$EndComp
Text GLabel 6000 6850 2    39   Input ~ 0
5V
Text GLabel 6000 6950 2    39   Input ~ 0
GND
Text GLabel 6100 3200 0    39   Input ~ 0
ENDL
Text GLabel 6100 3300 0    39   Input ~ 0
ENDP
$Comp
L Conn_02x04_Odd_Even J2
U 1 1 5A0833E7
P 1300 6575
F 0 "J2" H 1350 6775 50  0000 C CNN
F 1 "LED" H 1350 6275 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x04_Pitch2.54mm" H 1300 6575 50  0001 C CNN
F 3 "" H 1300 6575 50  0001 C CNN
	1    1300 6575
	-1   0    0    -1  
$EndComp
$Comp
L +5V #PWR019
U 1 1 5A083480
P 850 6425
F 0 "#PWR019" H 850 6275 50  0001 C CNN
F 1 "+5V" H 850 6565 50  0000 C CNN
F 2 "" H 850 6425 50  0001 C CNN
F 3 "" H 850 6425 50  0001 C CNN
	1    850  6425
	1    0    0    -1  
$EndComp
Text GLabel 1550 6475 2    39   Input ~ 0
Q5
Text GLabel 1550 6575 2    39   Input ~ 0
Q6
Text GLabel 1550 6675 2    39   Input ~ 0
Q7
Text GLabel 1550 6775 2    39   Input ~ 0
Q8
$Comp
L R_Small R16
U 1 1 5A085633
P 8150 3300
F 0 "R16" H 8180 3320 50  0000 L CNN
F 1 "0R" H 8180 3260 50  0000 L CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 8150 3300 50  0001 C CNN
F 3 "" H 8150 3300 50  0001 C CNN
	1    8150 3300
	0    1    1    0   
$EndComp
$Comp
L R_Small R17
U 1 1 5A085733
P 8150 3700
F 0 "R17" H 8180 3720 50  0000 L CNN
F 1 "0R" H 8180 3660 50  0000 L CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 8150 3700 50  0001 C CNN
F 3 "" H 8150 3700 50  0001 C CNN
	1    8150 3700
	0    1    1    0   
$EndComp
Text Notes 5225 4950 0    60   ~ 0
PA0\nPA1\nPA4\nPB0\nPC1\nPC0\n
Text Notes 3950 5025 0    60   ~ 0
PC2\nPC3\n\n
Text Notes 5700 5025 0    60   ~ 0
PA10\nPA2\nPA3\n\n
Text Notes 5750 4750 0    60   ~ 0
PB4\nPB5\n\n\n
Text Notes 5750 4350 0    60   ~ 0
PA9\n\n\n
Text Notes 5675 3625 0    60   ~ 0
PB8\nPB9\n\n\n\n
$Comp
L C_Small C10
U 1 1 5A0A7ED3
P 2300 3850
F 0 "C10" H 2310 3920 50  0000 L CNN
F 1 "100nF" H 2310 3770 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2300 3850 50  0001 C CNN
F 3 "" H 2300 3850 50  0001 C CNN
	1    2300 3850
	0    1    1    0   
$EndComp
$Comp
L +3.3V #PWR020
U 1 1 5A0A813C
P 2600 3825
F 0 "#PWR020" H 2600 3675 50  0001 C CNN
F 1 "+3.3V" H 2600 3965 50  0000 C CNN
F 2 "" H 2600 3825 50  0001 C CNN
F 3 "" H 2600 3825 50  0001 C CNN
	1    2600 3825
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 1550 6450 1550
Wire Wire Line
	6450 1450 6350 1450
Wire Wire Line
	6200 4900 6100 4900
Wire Wire Line
	6100 4800 6200 4800
Wire Wire Line
	4700 1750 4800 1750
Wire Wire Line
	4800 1850 4700 1850
Wire Wire Line
	4700 1650 4800 1650
Wire Wire Line
	4700 1550 4800 1550
Wire Wire Line
	4700 1450 4800 1450
Wire Wire Line
	4700 1350 4800 1350
Wire Wire Line
	4950 4100 4850 4100
Wire Wire Line
	4950 4000 4850 4000
Wire Wire Line
	4950 3900 4850 3900
Wire Wire Line
	1900 1300 2900 1300
Wire Wire Line
	2700 1300 2700 1450
Wire Wire Line
	8550 5450 8550 5650
Wire Wire Line
	8650 5550 8550 5550
Connection ~ 8550 5550
Wire Wire Line
	4950 3800 4850 3800
Wire Wire Line
	4950 3400 4850 3400
Wire Wire Line
	4950 3300 4850 3300
Wire Wire Line
	9100 4300 9100 4200
Wire Wire Line
	8000 1550 9100 1550
Wire Wire Line
	10500 2600 10300 2600
Wire Wire Line
	10300 2900 10500 2900
Connection ~ 10500 2900
Wire Wire Line
	10500 2600 10500 3000
Wire Wire Line
	9900 2900 10100 2900
Wire Wire Line
	9900 2600 10100 2600
Wire Wire Line
	8000 1800 8000 1850
Connection ~ 8000 1550
Wire Wire Line
	9100 1550 9100 1800
Wire Wire Line
	8000 1500 8000 1600
Wire Wire Line
	8000 2400 8000 2350
Wire Wire Line
	8000 2400 8300 2400
Wire Wire Line
	8000 2100 8300 2100
Wire Wire Line
	8000 2150 8000 2100
Wire Wire Line
	10200 2400 10200 2350
Wire Wire Line
	9900 2400 10200 2400
Wire Wire Line
	10200 2100 10200 2150
Wire Wire Line
	9900 2100 10200 2100
Wire Wire Line
	2450 5450 2350 5450
Wire Wire Line
	2450 5350 2350 5350
Wire Wire Line
	1400 1500 1400 1400
Wire Wire Line
	1400 1400 1300 1400
Wire Wire Line
	1700 1650 1700 1600
Wire Wire Line
	1700 2000 1700 1850
Wire Wire Line
	1300 1300 1500 1300
Wire Wire Line
	9050 5450 9050 5650
Wire Wire Line
	9550 5450 9550 5650
Wire Wire Line
	2250 1650 2250 1800
Wire Wire Line
	2250 1800 2700 1800
Wire Wire Line
	2700 1650 2700 2000
Wire Wire Line
	2250 1300 2250 1450
Connection ~ 2250 1300
Connection ~ 2700 1800
Connection ~ 2700 1300
Wire Wire Line
	9150 5550 9050 5550
Connection ~ 9050 5550
Wire Wire Line
	9650 5550 9550 5550
Connection ~ 9550 5550
Wire Wire Line
	1650 4550 1750 4550
Wire Wire Line
	1650 4850 1750 4850
Wire Wire Line
	1650 5150 1750 5150
Wire Wire Line
	1650 5450 1750 5450
Wire Wire Line
	2550 2800 2450 2800
Wire Wire Line
	2550 3000 2450 3000
Wire Wire Line
	2550 3200 2450 3200
Wire Wire Line
	1650 4750 1750 4750
Wire Wire Line
	2850 2800 2750 2800
Wire Wire Line
	2750 2900 2850 2900
Wire Wire Line
	2850 3000 2750 3000
Wire Wire Line
	2750 3100 2850 3100
Wire Wire Line
	1650 5050 1750 5050
Wire Wire Line
	1650 5350 1750 5350
Wire Wire Line
	1650 5650 1750 5650
Wire Wire Line
	1550 2800 1650 2800
Wire Wire Line
	1550 3000 1650 3000
Wire Wire Line
	1550 3200 1650 3200
Wire Wire Line
	2050 3850 2050 3800
Wire Wire Line
	2450 4550 2350 4550
Wire Wire Line
	2350 4750 2450 4750
Wire Wire Line
	2450 5150 2350 5150
Wire Wire Line
	2450 5650 2350 5650
Wire Wire Line
	2450 4850 2350 4850
Wire Wire Line
	2450 5050 2350 5050
Wire Wire Line
	6450 1650 6350 1650
Wire Wire Line
	6350 1750 6450 1750
Wire Wire Line
	5650 1550 5550 1550
Wire Wire Line
	5550 1650 5650 1650
Wire Wire Line
	5650 1750 5550 1750
Wire Wire Line
	5550 1850 5650 1850
Wire Wire Line
	6100 4700 6200 4700
Wire Wire Line
	6100 4100 6200 4100
Wire Wire Line
	4950 4700 4850 4700
Wire Wire Line
	4950 4600 4850 4600
Wire Wire Line
	4950 4500 4850 4500
Wire Wire Line
	4850 4400 4950 4400
Wire Wire Line
	6000 6000 5900 6000
Wire Wire Line
	1650 2900 1550 2900
Wire Wire Line
	1650 3100 1550 3100
Wire Wire Line
	1650 3300 1550 3300
Wire Wire Line
	2550 2900 2450 2900
Wire Wire Line
	2450 3100 2550 3100
Wire Wire Line
	2450 3300 2550 3300
Wire Wire Line
	2750 3300 2850 3300
Wire Wire Line
	2750 3200 2850 3200
Wire Wire Line
	10000 3700 9900 3700
Wire Wire Line
	10000 3300 9900 3300
Wire Wire Line
	4950 4900 4850 4900
Wire Wire Line
	4850 4800 4950 4800
Wire Wire Line
	6100 4500 6200 4500
Wire Wire Line
	2750 3500 2850 3500
Wire Wire Line
	2750 3400 2850 3400
Wire Wire Line
	2550 3400 2450 3400
Wire Wire Line
	1550 3400 1650 3400
Wire Wire Line
	1550 3500 1650 3500
Wire Wire Line
	5650 1350 5550 1350
Wire Wire Line
	5550 1450 5650 1450
Wire Wire Line
	1400 1100 1400 1300
Connection ~ 1400 1300
Wire Wire Line
	6200 4400 6100 4400
Wire Wire Line
	6000 5900 5900 5900
Wire Wire Line
	5900 5800 6000 5800
Wire Wire Line
	4700 5650 4700 5750
Connection ~ 4700 5650
Wire Wire Line
	4700 6050 4700 5950
Wire Wire Line
	4300 6050 4300 5950
Connection ~ 4300 5650
Wire Wire Line
	4600 5650 4900 5650
Wire Wire Line
	4150 5650 4400 5650
Wire Wire Line
	3850 5650 3950 5650
Wire Wire Line
	4300 5750 4300 5650
Wire Wire Line
	6000 7050 5900 7050
Wire Wire Line
	6000 6950 5900 6950
Wire Wire Line
	5900 6850 6000 6850
Wire Wire Line
	4700 6700 4700 6800
Connection ~ 4700 6700
Wire Wire Line
	4700 7100 4700 7000
Wire Wire Line
	4300 7100 4300 7000
Connection ~ 4300 6700
Wire Wire Line
	4600 6700 4900 6700
Wire Wire Line
	4150 6700 4400 6700
Wire Wire Line
	3850 6700 3950 6700
Wire Wire Line
	4300 6800 4300 6700
Wire Wire Line
	6100 3200 6200 3200
Wire Wire Line
	6100 3300 6200 3300
Wire Wire Line
	2450 3500 2550 3500
Wire Wire Line
	850  6425 850  6775
Wire Wire Line
	850  6775 1000 6775
Wire Wire Line
	1000 6675 850  6675
Connection ~ 850  6675
Wire Wire Line
	1000 6575 850  6575
Connection ~ 850  6575
Wire Wire Line
	1000 6475 850  6475
Connection ~ 850  6475
Wire Wire Line
	1550 6475 1500 6475
Wire Wire Line
	1550 6575 1500 6575
Wire Wire Line
	1550 6675 1500 6675
Wire Wire Line
	1550 6775 1500 6775
Wire Wire Line
	8250 3700 8300 3700
Wire Wire Line
	8050 3700 8000 3700
Wire Wire Line
	8000 3300 8050 3300
Wire Wire Line
	8250 3300 8300 3300
Wire Wire Line
	2600 3825 2600 3850
Wire Wire Line
	2600 3850 2400 3850
Wire Wire Line
	2200 3850 2075 3850
Wire Wire Line
	2075 3850 2075 3825
Wire Wire Line
	2075 3825 2050 3825
Connection ~ 2050 3825
$EndSCHEMATC
