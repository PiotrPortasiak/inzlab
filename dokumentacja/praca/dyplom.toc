\contentsline {chapter}{\numberline {1}Wst\IeC {\k e}p}{3}{chapter.1}
\contentsline {chapter}{\numberline {2}Cel i zakres pracy}{5}{chapter.2}
\contentsline {chapter}{\numberline {3}Analiza problemu}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Zastosowanie urz\IeC {\k a}dzenia}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}Optyczne sensory odleg\IeC {\l }o\IeC {\'s}ci}{7}{section.3.2}
\contentsline {section}{\numberline {3.3}Rodzaj nap\IeC {\k e}du}{10}{section.3.3}
\contentsline {section}{\numberline {3.4}Pozycjonowanie}{10}{section.3.4}
\contentsline {section}{\numberline {3.5}Kalibracja}{11}{section.3.5}
\contentsline {section}{\numberline {3.6}Komunikacja}{12}{section.3.6}
\contentsline {chapter}{\numberline {4}Koncepcja budowy stanowiska}{15}{chapter.4}
\contentsline {chapter}{\numberline {5}Konstrukcja mechaniczna}{17}{chapter.5}
\contentsline {section}{\numberline {5.1}Pozycjoner liniowy}{18}{section.5.1}
\contentsline {section}{\numberline {5.2}G\IeC {\l }owica pomiarowa}{19}{section.5.2}
\contentsline {section}{\numberline {5.3}Obudowa monta\IeC {\.z}owa}{24}{section.5.3}
\contentsline {chapter}{\numberline {6}Uk\IeC {\l }ad elektroniczny}{27}{chapter.6}
\contentsline {section}{\numberline {6.1}Modu\IeC {\l } mikrokontrolera}{29}{section.6.1}
\contentsline {section}{\numberline {6.2}Modu\IeC {\l } nadrz\IeC {\k e}dny (\emph {Master})}{31}{section.6.2}
\contentsline {section}{\numberline {6.3}Modu\IeC {\l } podrz\IeC {\k e}dny (\emph {Slave})}{32}{section.6.3}
\contentsline {section}{\numberline {6.4}Modu\IeC {\l } czujnik\IeC {\'o}w \emph {IR}}{33}{section.6.4}
\contentsline {section}{\numberline {6.5}Modu\IeC {\l } czujnik\IeC {\'o}w \emph {TR}}{34}{section.6.5}
\contentsline {section}{\numberline {6.6}Modu\IeC {\l } czujnik\IeC {\'o}w \emph {TOF} }{35}{section.6.6}
\contentsline {section}{\numberline {6.7}Modu\IeC {\l } \emph {Laser}}{36}{section.6.7}
\contentsline {section}{\numberline {6.8}Modu\IeC {\l } \emph {Endstop}}{36}{section.6.8}
\contentsline {chapter}{\numberline {7}Oprogramowanie wbudowane}{37}{chapter.7}
\contentsline {section}{\numberline {7.1}Konfiguracja modu\IeC {\l }u \emph {Master}}{38}{section.7.1}
\contentsline {section}{\numberline {7.2}Konfiguracja modu\IeC {\l }u \emph {Slave}}{40}{section.7.2}
\contentsline {section}{\numberline {7.3}Obs\IeC {\l }uga stanowiska pomiarowego}{42}{section.7.3}
\contentsline {section}{\numberline {7.4}Obs\IeC {\l }uga g\IeC {\l }owicy pomiarowej}{43}{section.7.4}
\contentsline {chapter}{\numberline {8}Weryfikacja urz\IeC {\k a}dzenia i eksperymenty}{45}{chapter.8}
\contentsline {section}{\numberline {8.1}Zakres i precyzja dzia\IeC {\l }ania}{45}{section.8.1}
\contentsline {section}{\numberline {8.2}Przebieg eksperymentu}{47}{section.8.2}
\contentsline {chapter}{\numberline {9}Podsumowanie i wnioski}{55}{chapter.9}
\contentsline {chapter}{\numberline {A}Schematy elektroniczne}{57}{appendix.A}
\contentsline {chapter}{Bibliografia}{88}{figure.A.40}
