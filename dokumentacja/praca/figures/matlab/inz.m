clear all;
close all;
clc;
load('data3.mat')
%% Wykres 1
figure(1);
hold on;
grid on;
title('Poziom napi�cia dla czujnik�w IR')
plot(i,F1_on-F1_off);
plot(i,F2_on-F2_off );
plot(i,F3_on-F3_off);
% plot(i,S1);
% plot(i,S2);
xlabel('Przemieszczenie (mm)');
ylabel('Warto�� (12-bit)');
legend('IR1 (pionowo)','IR2 (poziomo)','IR3 (poziomo)')
saveas(gcf,'charts/1.png')
%% Wykres 2
figure(2);
hold on;
grid on;
title('Poziom napi�cia dla czujnik�w SHARP')
plot(i,S1);
plot(i,S2);
xlabel('Przemieszczenie (mm)');
ylabel('Warto�� (12-bit)');
legend('SHARP1 (pionowo)','SHARP2 (poziomo)')
saveas(gcf,'charts/2.png')
%% Wykres 3
figure(3);
hold on;
grid on;
title('Odleg�o�� dla ST VL53L0X i VL6180X (18cm)')
plot(i,VL53_DIST);
plot(i,VL61_DIST);
xlabel('Przemieszczenie (mm)');
ylabel('Odleg�o�� (mm)');
legend('VL53L0X (ToF)','VL6180X (ToF)')
saveas(gcf,'charts/3.png')
%% Wykres 4
figure(4);
hold on;
grid on;
title('Nat�enie �wiat�a dla ST VL6180X (ALS)')
plot(i,VL61_ALS);
xlabel('Przemieszczenie (mm)');
ylabel('Nat�enie �wia�a (lx)');
legend('VL6180X (ALS)')
saveas(gcf,'charts/4.png')
%% Wykres 5
figure(5);
hold on;
grid on;
xlabel('Przemieszczenie (mm)');
ylabel('Warto�� (8-bit)');
title('Nat�enie �wiat�a dla ADPS-9660 (IR)')
plot(i,PROX);
legend('ADPS-9660(IR)')
saveas(gcf,'charts/5.png')
%% Wykres 6
figure(6);
hold on;
grid on;
xlabel('Przemieszczenie (mm)');
ylabel('Nat�enie �wia�a (lx)');
title('Nat�enie �wiat�a dla ADPS-9660 (ALS)')
plot(i,ADPS_ALS);
legend('ADPS-9660 (ALS)')
saveas(gcf,'charts/6.png')
%% Wykres 7
figure(7);
hold on;
grid on;
xlabel('Przemieszczenie (mm)');
ylabel('Nat�enie sk�adowej (lx)');
title('Kolor �wiat�a dla ADPS-9660 (RGB)')
%plot(i,ADPS_ALS+20,'k');
plot(i,R,'r');
plot(i,G,'g');
plot(i,B,'b');
legend('Czerwony (R)','Zielony (G)','Niebieski (B)');
saveas(gcf,'charts/7.png')

%% Wykres 8
figure(8);
hold on;
grid on;
title('Odleg�o�� dla ST VL53L0X i VL6180X (15cm)')
plot(i,VL53_DIST2);
plot(i,VL61_DIST2);
xlabel('Przemieszczenie (mm)');
ylabel('Odleg�o�� (mm)');
legend('VL53L0X (ToF)','VL6180X (ToF)')
saveas(gcf,'charts/8.png')


% %% Wykres 9
% figure(9);
% hold on;
% grid on;
% xlabel('Przemieszczenie (mm)');
% ylabel('Nat�enie sk�adowej (lx)');
% title('Kolor �wiat�a ADPS-9660 (RGB)')
% %plot(i,ADPS_ALS+20,'k');
% plot(i,R+G+B,'r');
% 
% legend('Czerwony (R)','Zielony (G)','Niebieski (B)');
% saveas(gcf,'charts/7.png')
% 

% % Wykres 8
% figure(8);
% hold on;
% grid on;
% xlabel('X[mm]');
% ylabel('');
% title('Kolor �wiat�a ADPS-9660 (RGB)')
% plot(i,15*VL61_ALS);
% plot(i,ADPS_ALS);
% legend('10*VL61_ALS','ADPS_ALS');


