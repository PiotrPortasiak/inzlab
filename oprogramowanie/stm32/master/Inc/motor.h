/*!
   \file   motor.h
   \author Piotr Portasiak
   \brief  This file provides functions which describe
   	   	   a single motor controller object.
 */

#ifndef MOTOR_H_
#define MOTOR_H_

/* Includes --------------------------------------------------------*/
#include "stm32f4xx.h"

/* Macros ----------------------------------------------------------*/
#define MICROSTEPS_HOMING	8 /**< microsteps divider for homing */
#define MAX_RANGE_X			450*100*MICROSTEPS_HOMING /**< axis range in steps */
#define VEL_START_HOMING	180*2 /**< min PSC for homing speed */
#define VEL_MAX_HOMING		180*2 /**< max PSC for homing speed */

#define MICROSTEPS			8 /**< microsteps divider */
#define VEL_START	(MICROSTEPS*95)-1 /**< max PSC for homing speed */
#define VEL_MAX		(MICROSTEPS*65)-1 /**< min PSC for homing speed */

/* Type definitions ------------------------------------------------*/

/*!
  Enumeration of motor movement direction
 */
typedef enum {
	CCW = 0, /**< counterclockwise turn */
	CW = !CCW, /**< clockwise turn*/
} MotorTurnDirection;

/*!
  Enumeration of motor state
 */
typedef enum {
	IDLE = 0, /**< motor is idle */
	ACCELERATE, /**< motor is accelerating */
	RUN, /**< motor is running */
	DECELERATE, /**< motor is decelerating */
} MotorState;

/*!
 Structure which represents stepper motor
 */
typedef struct {

	/*!
	 \name General
	 */
	/*\{*/
	uint8_t direction; /**< direction of rotation of the motor shaft \\
	(0=carriage moves to motor, 1=carriage moves away from motor) */
	uint32_t position; /**< total linear displacement on the axis (in steps)  */
	/*\}*/

	/*!
	  \name Speed profiler
	 */
	/*\{*/
	uint16_t vel_max;				/**< PSC for max speed */
	uint16_t vel_start;				/**< PSC for min speed */
	uint32_t steps_todo;			/**< Steps to do */
	uint32_t steps_done;			/**< Steps done */
	uint32_t steps_dec;				/**< Steps to decelerate */
	TIM_HandleTypeDef *htim_ramps; 	/**< Speed ramp timer */
	MotorState state;				/**< Motor state */
	/*\}*/

	/*!
	  \name Step
	 */
	/*\{*/
	TIM_HandleTypeDef *htim_step;	/**< Step timer */
	uint32_t htim_step_channel;		/**< Channel of step timer */
	/*\}*/

	/*!
	  \name EN
	 */
	/*\{*/
	uint8_t en;					/**< Motor torque state (0=Disabled, 1=Enabled) */
	GPIO_TypeDef *en_port;		/**< Motor torque state GPIO_port */
	uint16_t en_pin;			/**< Motor torque state	GPIO_pin */
	/*\}*/

	/*!
	  \name END_L
	 */
	/*\{*/
	uint8_t endl;				/**< Left Endstop state (0=Disabled, 1=Enabled) */
	GPIO_TypeDef *endl_port; 	/**< Left Endstop GPIO_port */
	uint16_t endl_pin;	 		/**< Left Endstop GPIO_pin */
	/*\}*/

	/*!
	  \name END_R
	 */
	/*\{*/
	uint8_t endr;				/**< Right Endstop state (0=Disabled, 1=Enabled) */
	GPIO_TypeDef *endr_port;	/**< Right Endstop GPIO_port */
	uint16_t endr_pin;			/**< Right Endstop GPIO_pin */
	/*\}*/

	/*!
	  \name DIR
	 */
	/*\{*/
	GPIO_TypeDef *dir_port;		/**< Direction GPIO_port */
	uint16_t dir_pin;			/**< Direction GPIO_pin */
	/*\}*/
} motor_t;


/* Functions headers -----------------------------------------------*/
void Motor_Init(motor_t *stepper, TIM_HandleTypeDef *htim_step,
		uint32_t tim_step_channel, TIM_HandleTypeDef *htim_ramps,
		GPIO_TypeDef *en_port, uint16_t en_pin,
		GPIO_TypeDef *dir_port, uint16_t dir_pin,
		GPIO_TypeDef *endl_port, uint16_t endl_pin,
		GPIO_TypeDef *endr_port, uint16_t endr_pin);

void Motor_Torque(motor_t *motor, uint8_t en);
void Motor_Stop(motor_t *motor);
void Motor_Start(motor_t *motor);
void Motor_Direction(motor_t *motor, uint8_t dir);
void Motor_Run(motor_t *motor, uint32_t steps);
void Motor_Homing(motor_t *motor, uint8_t dir);
void Motor_RunTo(motor_t *motor, uint8_t dir);
void Motor_RunAway(motor_t *motor, uint8_t dir);
void Motor_EmergencyStop(motor_t *motor, uint16_t GPIO_Pin);
void Motor_HandleUpdateEvent(motor_t *motor, TIM_HandleTypeDef *htim);


#endif /* MOTOR_H_ */
