/*!
   \file   communication.h
   \author Piotr Portasiak
   \brief  This file provides functions which describe the communication
*/

#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_

/* Includes --------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "usart.h"
#include "manipulator.h"

/* Macros ----------------------------------------------------------*/
#define BUFFER_SIZE 64	/**< Size of Tx/Rx USART buffers */

/* Functions headers -----------------------------------------------*/
void Communication_Init(void);
void Communication_Perform(void);
void HAL_UART_RxIdleCallback(UART_HandleTypeDef *huart);


#endif /* COMMUNICATION_H_ */
