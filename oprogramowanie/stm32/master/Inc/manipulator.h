/*!
   \file   manipulator.h
   \author Piotr Portasiak
   \brief  This file provides functions which describe the manipulator object.
*/

#ifndef MANIPULATOR_H_
#define MANIPULATOR_H_

/* Includes --------------------------------------------------------*/
#include "stm32f4xx.h"
#include "motor.h"

/* Type definitions ------------------------------------------------*/
typedef struct {
	motor_t motor;
} manipulator_t;

/* Functions headers -----------------------------------------------*/
void Manipulator_Init(void);
void Manipulator_SetEnable(uint8_t enable);
void Manipulator_Homing(uint8_t site);
void Manipulator_Move(int32_t steps, uint8_t dir);
uint8_t Manipulator_MoveTo(uint32_t x);
uint32_t Manipulator_GetPosition(void);
void Manipulator_setSpeed(uint8_t speed1,uint8_t speed2);

#endif /* MANIPULATOR_H_ */
