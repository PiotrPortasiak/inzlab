/*!
   \file   motor.c
   \author Piotr Portasiak
   \brief  This file provides functions which describe
   	   	   a single motor controller object.
 */

/* Includes --------------------------------------------------------*/

#include "motor.h"
#include <stdlib.h>

/* Functions -------------------------------------------------------*/

/*!
 \brief Motor initialization
 \param[in,out] stepper
 \param[in,out] htim_step
 \param[in] tim_step_channel
 \param[in,out] htim_ramps
 \param[in,out] en_port
 \param[in] en_pin
 \param[in,out] dir_port
 \param[in] dir_pin
 \param[in,out] endl_port
 \param[in] endl_pin
 \param[in,out] endr_port
 \param[in] endr_pin
 \retval None
 */
void Motor_Init(motor_t *motor, TIM_HandleTypeDef *htim_step,
		uint32_t tim_step_channel, TIM_HandleTypeDef *htim_ramps,
		GPIO_TypeDef *en_port, uint16_t en_pin,
		GPIO_TypeDef *dir_port, uint16_t dir_pin,
		GPIO_TypeDef *endl_port, uint16_t endl_pin,
		GPIO_TypeDef *endr_port, uint16_t endr_pin)
{
	/* Enable timer interrupts for step */
	__HAL_TIM_ENABLE_IT(htim_step, TIM_IT_UPDATE);
	/* Enable timer interrupts for ramps */
	__HAL_TIM_ENABLE_IT(htim_ramps, TIM_IT_UPDATE);

	motor->htim_step = htim_step;
	motor->htim_step_channel = tim_step_channel;
	motor->htim_ramps = htim_ramps;
	motor->en = 0;
	motor->en_port = en_port;
	motor->en_pin = en_pin;
	motor->direction = 0;
	motor->dir_port = dir_port;
	motor->dir_pin = dir_pin;
	motor->endl = HAL_GPIO_ReadPin(endl_port,endl_pin);
	motor->endl_port = endl_port;
	motor->endl_pin = endl_pin;
	motor->endr = HAL_GPIO_ReadPin(endr_port,endr_pin);
	motor->endr_pin = endr_pin;
	motor->endr = 0;
	motor->vel_start = VEL_START;
	motor->vel_max = VEL_MAX;
	motor->steps_done = 0;
	motor->steps_todo = 0;
	motor->steps_dec = 0;
	motor->position=0;
	motor->state = IDLE;
}

/*!
 \brief Motor torque control

  Enable/Disable torque for motor
  \param[in,out] motor Stepper Motor
  \param[in] en State (0=Disabled, 1=Enabled)
  \retval None
 */
void Motor_Torque(motor_t *motor, FunctionalState en)
{
	motor->en=en;
	HAL_GPIO_WritePin(motor->en_port, motor->en_pin, motor->en);
}

/*!
 \brief Motor direction control

  Set turn direction for motor.
  \param[in,out] motor Stepper Motor
  \param[in] direction (0=ToMotor, 1=AwayFrom)
  \retval None
 */
void Motor_Direction(motor_t *motor, MotorTurnDirection direction)
{
	motor->direction = direction;
	HAL_GPIO_WritePin(motor->dir_port, motor->dir_pin, motor->direction);
}

/*!
 \brief Motor start

  Start motor
  \param[in,out] motor Stepper Motor
  \retval None
 */
void Motor_Start(motor_t *motor)
{
	Motor_Torque(motor,ENABLE);
	HAL_TIM_PWM_Start_IT(motor->htim_step, motor->htim_step_channel);
	HAL_TIM_Base_Start_IT(motor->htim_ramps);
}

/*!
 \brief Motor stop

  Stop motor
  \param[in,out] motor Stepper Motor
  \retval None
 */
void Motor_Stop(motor_t *motor)
{
	HAL_TIM_PWM_Stop_IT(motor->htim_step, motor->htim_step_channel);
	HAL_TIM_Base_Stop_IT(motor->htim_ramps);
	motor->steps_done = 0;
	motor->steps_todo = 0;
	motor->steps_dec = 0;
	motor->state = IDLE;
}

/*!
 \brief Motor emergency stop

  Motor emergency stop
  \param[in,out] motor Stepper Motor
  \param[in] GPIO_Pin
  \retval None
 */
void Motor_EmergencyStop(motor_t *motor, uint16_t GPIO_Pin)
{
	Motor_Torque(motor, DISABLE);
	Motor_Stop(motor);

	if(GPIO_Pin == ENDL_Pin)
		motor->endl=ENABLE;

	if(GPIO_Pin == ENDP_Pin)
		motor->endr=ENABLE;
}

/*!
 \brief Motor homing

  Motor homing
  \param[in,out] motor Stepper Motor
  \param[in] dir
  \retval None
 */
void Motor_Homing(motor_t *motor, uint8_t site)
{

	Motor_RunTo(motor,site);
	HAL_Delay(100);
	Motor_RunAway(motor,site);
	motor->position=0;
}

/*!
 \brief Motor run

  Motor homing
  \param[in,out] motor Stepper Motor
  \param[in] steps
  \retval None
 */
void Motor_Run(motor_t *motor, uint32_t steps)
{
	/* Already on this position (no error)*/
	if(steps==0) return;
	motor->steps_todo = steps;
	motor->htim_step->Instance->PSC = motor->vel_start;
	motor->steps_dec = steps/2; // ramps
	motor->state = ACCELERATE;

	Motor_Start(motor);

	while(motor->state != IDLE)
	{

		if (HAL_GPIO_ReadPin(ENDL_GPIO_Port,ENDL_Pin))
			Motor_EmergencyStop(motor,ENDL_Pin);

		if (HAL_GPIO_ReadPin(ENDP_GPIO_Port,ENDP_Pin))
			Motor_EmergencyStop(motor,ENDP_Pin);

		HAL_Delay(1);
	}
}

/*!
 \brief Motor RunTo

  Motor homing
  \param[in,out] motor Stepper Motor
  \param[in] dir
  \retval None
 */
void Motor_RunTo(motor_t *motor, uint8_t dir)
{
	/* Set direction for move */
	/*if(site==1)	Motor_Direction(motor, 1);
	else Motor_Direction(motor, 0);*/

	Motor_Direction(motor, dir);

	motor->steps_todo = 2*MAX_RANGE_X;
	motor->htim_step->Instance->PSC = motor->vel_start;
	motor->steps_dec = (2*MAX_RANGE_X)/2; // ramps
	motor->state = ACCELERATE;

	Motor_Start(motor);

	while(motor->state != IDLE)
	{
		if (HAL_GPIO_ReadPin(ENDL_GPIO_Port,ENDL_Pin)==1)
		{
			motor->endl=1;
			return;
		}

		HAL_Delay(1);
	}

}

/*!
 \brief Motor RunAway

  Motor homing
  \param[in,out] motor Stepper Motor
  \param[in] dir
  \retval None
 */
void Motor_RunAway(motor_t *motor, uint8_t dir)
{
	/* Set direction for move */

	/*if(site==1)	Motor_Direction(motor, 0);
	else Motor_Direction(motor, 1);*/
	Motor_Direction(motor, !dir);

	motor->steps_todo = MAX_RANGE_X;
	motor->htim_step->Instance->PSC = motor->vel_start;
	motor->steps_dec = (2*MAX_RANGE_X)/2;// ramps
	motor->state = ACCELERATE;

	Motor_Start(motor);

	while(motor->state != IDLE)
	{
		if (HAL_GPIO_ReadPin(ENDL_GPIO_Port,ENDL_Pin)==0)
		{
			motor->endl=0;
			return;
		}
		HAL_Delay(1);
	};

}

/*!
 \brief Motor speed profiler

  Motor homing
  \param[in,out] motor Stepper Motor
  \param[in] htim timer
  \retval None
 */
void Motor_HandleUpdateEvent(motor_t *motor, TIM_HandleTypeDef *htim)
{
	if(motor->state == RUN) {
		//HAL_GPIO_WritePin(LED4_GPIO_Port,LED4_Pin,DISABLE);
		//HAL_GPIO_WritePin(LED3_GPIO_Port,LED3_Pin,DISABLE);
		//HAL_GPIO_WritePin(LED1_GPIO_Port,LED1_Pin,ENABLE);
	}
	if(htim->Instance == motor->htim_step->Instance)
	{
		// count steps
		motor->steps_done++;

		// actual position
		if(motor->direction) motor->position++;
		else motor->position--;

		// determine when decelerate
		if(motor->steps_done == motor->steps_dec)
		{
			motor->state = DECELERATE;
			HAL_TIM_Base_Start_IT(motor->htim_ramps);
		}

		// determine when stop
		if(motor->steps_done == motor->steps_todo)
		{
			Motor_Stop(motor);
		}
	}


	// speed ramp generator
	if(htim->Instance == motor->htim_ramps->Instance)
	{
		// if decelerate
		if(motor->state == ACCELERATE)
		{
			//HAL_GPIO_WritePin(LED4_GPIO_Port,LED4_Pin,DISABLE);
			//HAL_GPIO_WritePin(LED1_GPIO_Port,LED1_Pin,DISABLE);
			//HAL_GPIO_WritePin(LED3_GPIO_Port,LED3_Pin,ENABLE);
			motor->htim_step->Instance->PSC--;
			if(motor->htim_step->Instance->PSC <= motor->vel_max)
			{
				HAL_TIM_Base_Stop_IT(motor->htim_ramps);
				motor->steps_dec = motor->steps_todo-motor->steps_done;
				motor->state = RUN;
			}
		}
		if(motor->state == DECELERATE) {
			//HAL_GPIO_WritePin(LED4_GPIO_Port,LED4_Pin,ENABLE);
			//HAL_GPIO_WritePin(LED3_GPIO_Port,LED3_Pin,DISABLE);
			//HAL_GPIO_WritePin(LED1_GPIO_Port,LED1_Pin,DISABLE);
			motor->htim_step->Instance->PSC++;
			if(motor->htim_step->Instance->PSC >= motor->vel_start)
			{
				HAL_TIM_Base_Stop_IT(motor->htim_ramps);
			}
		}
	}
}
