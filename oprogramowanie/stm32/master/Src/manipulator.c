/*!
   \file   manipulator.c
   \author Piotr Portasiak
   \brief  This file provides functions which describe the manipulator object.
 */

/* Includes --------------------------------------------------------*/
#include "manipulator.h"
#include "stdlib.h"
#include "gpio.h"
#include "tim.h"

/* Local variables -------------------------------------------------*/
manipulator_t manip; /**< Manipulator structure */

/* Functions -------------------------------------------------------*/

/*!
  \brief Manipulator initialization
  \param None
  \retval None
 */
void Manipulator_Init(void)
{
	Motor_Init(&manip.motor, &htim2, TIM_CHANNEL_1, &htim10,
			EN_GPIO_Port, EN_Pin, DIR_GPIO_Port, DIR_Pin,
			ENDL_GPIO_Port, ENDL_Pin, ENDP_GPIO_Port,ENDP_Pin);
}

/*!
  \brief Movement given by number of steps

  Manipulator movement given by number of steps
  in specified direction.
  \param[in] steps Number of steps to do
  \param[in] dir Direction of movement
  \retval None
 */
void Manipulator_Move(int32_t steps, uint8_t dir)
{

	Motor_Direction(&manip.motor, dir);
	Motor_Run(&manip.motor, steps);

}


/*!
  \brief Movement given by position on the axis

  Manipulator movement given by position on the axis.
  In this case, the direction of movement is always known.
  \param[in] position Position on axis
  \retval Manipulator_Move_Error
 */
uint8_t Manipulator_MoveTo(uint32_t position)
{
	/* Check whether the desired position is not out of range */
	if((manip.motor.position + position) > MAX_RANGE_X)
		return 1;

	/* Move manipulator to specified position on the axis */

	/*
	if (manip.motor.position >= position)
		Manipulator_Move(position*MICROSTEPS - manip.motor.position, 1);
	else
		Manipulator_Move(manip.motor.position-position*MICROSTEPS, 0);
	 */

	/* Above statements simplified */
	Manipulator_Move(abs(position*MICROSTEPS > manip.motor.position),
			(position >= manip.motor.position));

	return 0;
}


/*!
  \brief Get the current position of manipulator

  Get current position of manipulator on the axis
  \param None
  \retval Current position of manipulator on the axis
 */
uint32_t Manipulator_GetPosition(void)
{
	return manip.motor.position;
}


/*!
  \brief Get the current position of manipulator

  Get current position of manipulator on the axis
  \param None
  \retval Current position of manipulator on the axis
 */
void Manipulator_setSpeed(uint8_t speed1,uint8_t speed2)
{
	//start 95 max 65
	manip.motor.vel_start = (8*speed1)-1;
	manip.motor.vel_max= (8*speed2)-1;
}

/*!
 \brief Enable or disable manipulator

  Enable or disable manipulator
  \param[in] enable Enable(1) or Disable(0)
  \retval None
 */
void Manipulator_State(uint8_t state)
{
	Motor_Torque(&manip.motor, state);
}

/*!
 \brief Move to the reference position

  Move the manipulator to the reference position on the axis
  \param[in] site Left_Motor(0) or Right_End(1)
  \retval None
 */
void Manipulator_Homing(uint8_t site)
{
	Manipulator_setSpeed(95,45);
	Motor_Homing(&manip.motor, site);
	Motor_Stop(&manip.motor);
	Manipulator_setSpeed(95,65);
}


/*!
   \brief  EXTI line detection callbacks.
   \param[in]  GPIO_Pin Specifies the pins connected EXTI line
   \retval None
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	/* Callback raised after exceeding the range of movement */
	Motor_EmergencyStop(&manip.motor, GPIO_Pin);
}


/*!
   \brief  Period elapsed callback in non blocking mode
   \param[in]  htim pointer to a TIM_HandleTypeDef structure that contains
               the configuration information for TIM module.
   \retval None
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	/* Callback raised after completing a single step */
	Motor_HandleUpdateEvent(&manip.motor, htim);
}
