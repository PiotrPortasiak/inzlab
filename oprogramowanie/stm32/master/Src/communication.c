/*!
   \file   communication.c
   \author Piotr Portasiak
   \brief  This file provides functions which describe the communication.
 */

/* Includes --------------------------------------------------------*/
#include "communication.h"

/* External variables ----------------------------------------------*/
extern DMA_HandleTypeDef hdma_usart1_rx;  /**< usart1_rx handler */
extern DMA_HandleTypeDef hdma_usart2_rx;  /**< usart2_rx handler */

/* Local variables -------------------------------------------------*/
uint8_t rx1_recv = RESET;  /**< recv from SLAVE flag  */
uint8_t rx1_buffer[BUFFER_SIZE] = {0};  /**< SLAVE rx_buffer  */
uint32_t rx1_len = 0;  /**< how many bytes recv from SLAVE */

uint8_t rx2_recv = RESET;  /**< recv from USER flag  */
uint8_t rx2_buffer[BUFFER_SIZE] = {0};  /**< USER rx_buffer  */
uint32_t rx2_len = 0;  /**< how many bytes recv from USER */

/* Functions -------------------------------------------------------*/

/*!
  \brief Communication initialization
  \param None
  \retval None
 */
void Communication_Init(void)
{
	/* Enable/Disable interrupts from SLAVE (USART1) */
	__HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);
	__HAL_DMA_DISABLE_IT (&hdma_usart1_rx, DMA_IT_TC);
	__HAL_DMA_DISABLE_IT (&hdma_usart1_rx, DMA_IT_HT);

	/* Start listen from SLAVE */
	HAL_UART_Receive_DMA(&huart1, rx1_buffer, sizeof(rx1_buffer));

	/* Enable/Disable interrupts from USER (USART2) */
	__HAL_UART_ENABLE_IT(&huart2, UART_IT_IDLE);
	__HAL_DMA_DISABLE_IT (&hdma_usart2_rx, DMA_IT_TC);
	__HAL_DMA_DISABLE_IT (&hdma_usart2_rx, DMA_IT_HT);

	/* Start listen from USER */
	HAL_UART_Receive_DMA(&huart2, rx2_buffer, sizeof(rx2_buffer));
}


/*!
   \brief USART_RX idle line detection callback.
   \param[in,out] huart pointer to a UART_HandleTypeDef structure that contains
 				the configuration information for the specified UART module.
   \retval None
 */
void HAL_UART_RxIdleCallback(UART_HandleTypeDef *huart)
{
	/* Set the frame received from SLAVE flag. */
	if(huart->Instance == USART1)
	{
		rx1_recv=SET;
		HAL_GPIO_TogglePin(LED1_GPIO_Port,LED1_Pin);
	}

	/* Set the frame received from USER flag. */
	if(huart->Instance == USART2)
	{
		rx2_recv=SET;
		HAL_GPIO_TogglePin(LED2_GPIO_Port,LED2_Pin);
	}
}


/*!
  \brief Communication Perform
  \param None
  \retval None
 */
void Communication_Perform(void)
{
	int32_t step = 0;
	/* Check whether a frame of bytes received from USER (blocking mode). */
	if(rx2_recv)
	{
		/* Calculate how many bytes received in the frame */
		rx2_len = sizeof(rx2_buffer)-hdma_usart2_rx.Instance->NDTR;

		/* Check whether the packet contains at least 2 bytes
		 * and start recognize commands for MASTER (frame parsing). */
		if(rx2_len>=2)
		{
			/* Move the manipulator to reference point. */

			switch(rx2_buffer[0])
			{
			case 'B':
			{
				Manipulator_Homing(rx2_buffer[1]);
				printf("----------|\r\n");
				printf("%d:%d\r\n",'B',0);
				printf("----------O\r\n");
				break;
			}
			case 'R':
			{

				step = ((rx2_buffer[4] & 0xff) << 24) | ((rx2_buffer[3] & 0xff) << 16) |
						((rx2_buffer[2] & 0xff) << 8)  | (rx2_buffer[1] & 0xff);


				Manipulator_Move(step,rx2_buffer[5]);
				printf("----------|\r\n");
				printf("%d:%d\r\n",'R', Manipulator_GetPosition());
				printf("----------O\r\n");

				break;

			}

			case 'Q':
				if(rx2_buffer[0]=='Q')
				{
					printf("----------|\r\n");
					printf("%d:%d\r\n",'Q', Manipulator_GetPosition());
					printf("----------O\r\n");
					break;
				}
			default: HAL_UART_Transmit(&huart1,rx2_buffer, rx2_len, 0xFFFF);
			}
			//
			//			if(rx2_buffer[0]=='B')
			//			{
			//				Manipulator_Homing(rx2_buffer[1]);
			//				printf("----------|\r\n");
			//				printf("%d:%d\r\n",'B',0);
			//				printf("----------O\r\n");
			//			}
			//			/* Move the manipulator with desired number of steps
			//			 * and in specified direction. */
			//			if(rx2_buffer[0]=='R')
			//			{
			//
			//				step = ((rx2_buffer[4] & 0xff) << 24) | ((rx2_buffer[3] & 0xff) << 16) |
			//						((rx2_buffer[2] & 0xff) << 8)  | (rx2_buffer[1] & 0xff);
			//
			//				Manipulator_Move(step,rx2_buffer[5]);
			//				printf("----------|\r\n");
			//				printf("%d:%d\r\n",'R', Manipulator_GetPosition());
			//				printf("----------O\r\n");
			//
			//			}
			//			if(rx2_buffer[0]=='Q')
			//			{
			//				printf("----------|\r\n");
			//				printf("%d:%d\r\n",'Q', Manipulator_GetPosition());
			//				printf("----------O\r\n");
			//			}

			/* Otherwise forward frame to SLAVE. */
			//			else
			//			{
			//				HAL_UART_Transmit(&huart1,rx2_buffer, rx2_len, 0xFFFF);
			//			}
		}

		/* Reset the frame received from USER flag
		 * and start listen from USER (blocking mode). */
		rx2_recv = RESET;
		HAL_UART_Receive_DMA(&huart2, rx2_buffer, sizeof(rx2_buffer));
	}

	/* Check whether a frame of bytes received from SLAVE (blocking mode). */
	if(rx1_recv)
	{
		/* Calculate how many bytes received in the frame */
		rx1_len = sizeof(rx1_buffer)-hdma_usart1_rx.Instance->NDTR;

		/* Check whether the packet contains at least 1 byte
		 * and directly forward it from SLAVE to USER. */
		if(rx1_len)
		{
			HAL_UART_Transmit(&huart2,rx1_buffer, rx1_len, 0xFFFF);
		}

		/* Reset the frame received from SLAVE flag
		 * and start listen from SLAVE (blocking mode). */
		rx1_recv = RESET;
		HAL_UART_Receive_DMA(&huart1, rx1_buffer, sizeof(rx1_buffer));
	}
}


/*!
  \brief Implementation of printf() function

  This is the custom implementation of printf() function
  \param[in] file
  \param[in] ptr
  \param[in] len
  \retval len
 */
int _write (int file, char *ptr, int len)
{
	/* Send message to USER using printf() */
	HAL_UART_Transmit(&huart2, (uint8_t *) ptr, len, 0xFFFF);
	return len;
}
