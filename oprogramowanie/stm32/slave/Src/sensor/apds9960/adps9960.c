/**
 * @file    SparkFun_APDS-9960.cpp
 * @brief   Library for the SparkFun APDS-9960 breakout board
 * @author  Shawn Hymel (SparkFun Electronics)
 *
 * @copyright	This code is public domain but you buy me a beer if you use
 * this and we meet someday (Beerware license).
 *
 * This library interfaces the Avago APDS-9960 to Arduino over I2C. The library
 * relies on the Arduino Wire (I2C) library. to use the library, instantiate an
 * APDS9960 object, call init(), and call the appropriate functions.
 *
 * APDS-9960 current draw tests (default parameters):
 *   Off:                   1mA
 *   Waiting for gesture:   14mA
 *   Gesture in progress:   35mA
 */



#include "apds9960/apds9960.h"
#include "i2c.h"







/**
 * @brief Takes one measurement of proximity
 *
 *
 */
uint8_t APDS9960_Proximity_Measure(uint8_t *value)
{
	uint8_t val;
	if ( ! readProximity(&val) )
		return 1;
	else *value = val;

	return 0;
}


/**
 * @brief Prepare to proximity measurements
 *
 *
 */
uint8_t APDS9960_Proximity_Config(void)
{
	/* Adjust LED current boost */
	if ( !setLEDBoost(3) )
		return 0;

	/* Adjust the Proximity sensor gain */
	if ( !setProximityGain(PGAIN_2X) )
		return 0;


	/* Enable Proximity sensor */
	if ( !enableProximitySensor(0) )
		return 0;



	return 1;


}

uint8_t APDS9960_ALS_RGB_Config(void)
{

	/* Enable Proximity sensor */
	if ( !enableLightSensor(0) )
		return 1;

	return 0;
}
uint8_t APDS9960_ALS_RGB_Measure(uint16_t *ambient, uint16_t *red, uint16_t *green, uint16_t *blue)
{
	uint16_t a=0,r=0,g=0,b=0;

	if ( ! readAmbientLight(&a) )
		return 1;
	else *ambient = a;
	if ( ! readRedLight(&r) )
		return 1;
	else *red = r;
	if ( ! readGreenLight(&g) )
		return 1;
	*green = g;
	if ( ! readBlueLight(&b) )
		return 1;
	else *blue = b;
	return 0;
}

/**
 * @brief Turn of the sensor
 *
 *
 */
void APDS9960_Deinit(void)
{
	/* Power down the sensor */
	HAL_GPIO_WritePin(EN3_GPIO_Port,EN3_Pin,GPIO_PIN_RESET);
}

/**
 * @brief Configures I2C communications and initializes registers to defaults
 *
 * @return 1 if initialized successfully. 0 otherwise.
 */
uint8_t APDS9960_Init(void)
{
	/* Power up the sensor */
	HAL_GPIO_WritePin(EN3_GPIO_Port,EN3_Pin,GPIO_PIN_SET);

	uint8_t id = 0;
	/* Read ID register and check against known values for APDS-9960 */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_ID, &id) ) {
		return 0;
	}

	if( !(id == APDS9960_ID_1 || id == APDS9960_ID_2) ) {
		return 0;
	}

	/* Set ENABLE register to 0 (disable all features) */
	if( !setMode(ALL, OFF) ) {
		return 0;
	}

	/* Set default values for ambient light and proximity registers */
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_ATIME, DEFAULT_ATIME) ) {
		return 0;
	}
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_WTIME, DEFAULT_WTIME) ) {
		return 0;
	}
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_PPULSE, DEFAULT_PROX_PPULSE) ) {
		return 0;
	}
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_POFFSET_UR, DEFAULT_POFFSET_UR) ) {
		return 0;
	}
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_POFFSET_DL, DEFAULT_POFFSET_DL) ) {
		return 0;
	}
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_CONFIG1, DEFAULT_CONFIG1) ) {
		return 0;
	}
	if( !setLEDDrive(DEFAULT_LDRIVE) ) {
		return 0;
	}
	if( !setProximityGain(DEFAULT_PGAIN) ) {
		return 0;
	}
	if( !setAmbientLightGain(DEFAULT_AGAIN) ) {
		return 0;
	}
	if( !setProxIntLowThresh(DEFAULT_PILT) ) {
		return 0;
	}
	if( !setProxIntHighThresh(DEFAULT_PIHT) ) {
		return 0;
	}
	if( !setLightIntLowThreshold(DEFAULT_AILT) ) {
		return 0;
	}
	if( !setLightIntHighThreshold(DEFAULT_AIHT) ) {
		return 0;
	}
	if( I2C_WriteByte(APDS9960_I2C_ADDR,APDS9960_PERS, DEFAULT_PERS) ) {
		return 0;
	}
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_CONFIG2, DEFAULT_CONFIG2) ) {
		return 0;
	}
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_CONFIG3, DEFAULT_CONFIG3) ) {
		return 0;
	}

	/* Set default values for gesture sense registers */
	if( !setGestureEnterThresh(DEFAULT_GPENTH) ) {
		return 0;
	}
	if( !setGestureExitThresh(DEFAULT_GEXTH) ) {
		return 0;
	}
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_GCONF1, DEFAULT_GCONF1) ) {
		return 0;
	}
	if( !setGestureGain(DEFAULT_GGAIN) ) {
		return 0;
	}
	if( !setGestureLEDDrive(DEFAULT_GLDRIVE) ) {
		return 0;
	}
	if( !setGestureWaitTime(DEFAULT_GWTIME) ) {
		return 0;
	}
	if( I2C_WriteByte(APDS9960_I2C_ADDR,APDS9960_GOFFSET_U, DEFAULT_GOFFSET) ) {
		return 0;
	}
	if( I2C_WriteByte(APDS9960_I2C_ADDR,APDS9960_GOFFSET_D, DEFAULT_GOFFSET) ) {
		return 0;
	}
	if( I2C_WriteByte(APDS9960_I2C_ADDR,APDS9960_GOFFSET_L, DEFAULT_GOFFSET) ) {
		return 0;
	}
	if( I2C_WriteByte(APDS9960_I2C_ADDR,APDS9960_GOFFSET_R, DEFAULT_GOFFSET) ) {
		return 0;
	}
	if( I2C_WriteByte(APDS9960_I2C_ADDR,APDS9960_GPULSE, DEFAULT_GPULSE) ) {
		return 0;
	}
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_GCONF3, DEFAULT_GCONF3) ) {
		return 0;
	}
	if( !setGestureIntEnable(DEFAULT_GIEN) ) {
		return 0;
	}
	//
	//#if 0
	//    /* Gesture config register dump */
	//    uint8_t reg;
	//    uint8_t val;
	//
	//    for(reg = 0x80; reg <= 0xAF; reg++) {
	//        if( (reg != 0x82) && \
	//            (reg != 0x8A) && \
	//            (reg != 0x91) && \
	//            (reg != 0xA8) && \
	//            (reg != 0xAC) && \
	//            (reg != 0xAD) )
	//        {
	//            wireReadDataByte(reg, &val);
	//            Serial.print(reg, HEX);
	//            Serial.print(": 0x");
	//            Serial.println(val, HEX);
	//        }
	//    }
	//
	//    for(reg = 0xE4; reg <= 0xE7; reg++) {
	//        wireReadDataByte(reg, &val);
	//        Serial.print(reg, HEX);
	//        Serial.print(": 0x");
	//        Serial.println(val, HEX);
	//    }
	//#endif

	return 1;
}



//
///*******************************************************************************
// * Public methods for controlling the APDS-9960
// ******************************************************************************/
//
///**
// * @brief Reads and returns the contents of the ENABLE register
// *
// * @return Contents of the ENABLE register. 0xFF if error.
// */
uint8_t getMode()
{
	uint8_t enable_value=0;

	/* Read current ENABLE register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_ENABLE, &enable_value) ) {
		return ERROR;
	}

	return enable_value;
}
//
///**
// * @brief Enables or disables a feature in the APDS-9960
// *
// * @param[in] mode which feature to enable
// * @param[in] enable ON (1) or OFF (0)
// * @return 1 if operation success. 0 otherwise.
// */
uint8_t setMode(uint8_t mode, uint8_t enable)
{
	uint8_t reg_val;

	/* Read current ENABLE register */
	reg_val = getMode();
	if( reg_val == ERROR ) {
		return 0;
	}

	/* Change bit(s) in ENABLE register */
	enable = enable & 0x01;
	if( mode >= 0 && mode <= 6 ) {
		if (enable) {
			reg_val |= (1 << mode);
		} else {
			reg_val &= ~(1 << mode);
		}
	} else if( mode == ALL ) {
		if (enable) {
			reg_val = 0x7F;
		} else {
			reg_val = 0x00;
		}
	}

	/* Write value back to ENABLE register */
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_ENABLE, &reg_val) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Starts the light (R/G/B/Ambient) sensor on the APDS-9960
 *
 * @param[in] interrupts 1 to enable hardware interrupt on high or low light
 * @return 1 if sensor enabled correctly. 0 on error.
 */
uint8_t enableLightSensor(uint8_t interrupts)
{

	/* Set default gain, interrupts, enable power, and enable sensor */
	if( !setAmbientLightGain(DEFAULT_AGAIN) ) {
		return 0;
	}
	if( interrupts ) {
		if( !setAmbientLightIntEnable(1) ) {
			return 0;
		}
	} else {
		if( !setAmbientLightIntEnable(0) ) {
			return 0;
		}
	}
	if( !enablePower() ){
		return 0;
	}
	if( !setMode(AMBIENT_LIGHT, 1) ) {
		return 0;
	}

	return 1;

}

/**
 * @brief Ends the light sensor on the APDS-9960
 *
 * @return 1 if sensor disabled correctly. 0 on error.
 */
uint8_t disableLightSensor()
{
	if( !setAmbientLightIntEnable(0) ) {
		return 0;
	}
	if( !setMode(AMBIENT_LIGHT, 0) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Starts the proximity sensor on the APDS-9960
 *
 * @param[in] interrupts 1 to enable hardware external interrupt on proximity
 * @return 1 if sensor enabled correctly. 0 on error.
 */
uint8_t enableProximitySensor(uint8_t interrupts)
{
	/* Set default gain, LED, interrupts, enable power, and enable sensor */
	if( !setProximityGain(DEFAULT_PGAIN) ) {
		return 0;
	}
	if( !setLEDDrive(DEFAULT_LDRIVE) ) {
		return 0;
	}
	if( interrupts ) {
		if( !setProximityIntEnable(1) ) {
			return 0;
		}
	} else {
		if( !setProximityIntEnable(0) ) {
			return 0;
		}
	}
	if( !enablePower() ){
		return 0;
	}
	if( !setMode(PROXIMITY, 1) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Ends the proximity sensor on the APDS-9960
 *
 * @return 1 if sensor disabled correctly. 0 on error.
 */
uint8_t disableProximitySensor()
{
	if( !setProximityIntEnable(0) ) {
		return 0;
	}
	if( !setMode(PROXIMITY, 0) ) {
		return 0;
	}

	return 1;
}



/**
 * Turn the APDS-9960 on
 *
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t enablePower()
{
	if( !setMode(POWER, 1) ) {
		return 0;
	}

	return 1;
}

/**
 * Turn the APDS-9960 off
 *
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t disablePower()
{
	if( !setMode(POWER, 0) ) {
		return 0;
	}

	return 1;
}

/*******************************************************************************
 * Ambient light and color sensor controls
 ******************************************************************************/

/**
 * @brief Reads the ambient (clear) light level as a 16-bit value
 *
 * @param[out] val value of the light sensor.
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t readAmbientLight(uint16_t *val)
{
	uint8_t val_byte;
	*val = 0;

	/* Read value from clear channel, low byte register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_CDATAL, &val_byte) ) {
		return 0;
	}
	*val = val_byte;

	/* Read value from clear channel, high byte register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_CDATAH, &val_byte) ) {
		return 0;
	}
	*val = *val + ((uint16_t)val_byte << 8);

	return 1;
}

/**
 * @brief Reads the red light level as a 16-bit value
 *
 * @param[out] val value of the light sensor.
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t readRedLight(uint16_t *val)
{
	uint8_t val_byte;
	*val = 0;

	/* Read value from clear channel, low byte register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_RDATAL, &val_byte) ) {
		return 0;
	}
	*val = val_byte;

	/* Read value from clear channel, high byte register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_RDATAH, &val_byte) ) {
		return 0;
	}
	*val = *val + ((uint16_t)val_byte << 8);

	return 1;
}

/**
 * @brief Reads the green light level as a 16-bit value
 *
 * @param[out] val value of the light sensor.
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t readGreenLight(uint16_t *val)
{
	uint8_t val_byte;
	*val = 0;

	/* Read value from clear channel, low byte register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_GDATAL, &val_byte) ) {
		return 0;
	}
	*val = val_byte;

	/* Read value from clear channel, high byte register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_GDATAH, &val_byte) ) {
		return 0;
	}
	*val = *val + ((uint16_t)val_byte << 8);

	return 1;
}

/**
 * @brief Reads the red light level as a 16-bit value
 *
 * @param[out] val value of the light sensor.
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t readBlueLight(uint16_t *val)
{
	uint8_t val_byte;
	*val = 0;

	/* Read value from clear channel, low byte register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_BDATAL, &val_byte) ) {
		return 0;
	}
	*val = val_byte;

	/* Read value from clear channel, high byte register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_BDATAH, &val_byte) ) {
		return 0;
	}
	*val = *val + ((uint16_t)val_byte << 8);

	return 1;
}

/*******************************************************************************
 * Proximity sensor controls
 ******************************************************************************/

/**
 * @brief Reads the proximity level as an 8-bit value
 *
 * @param[out] val value of the proximity sensor.
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t readProximity(uint8_t *val)
{

	uint8_t temp;
	/* Read value from proximity data register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_PDATA, &temp) ) {
		return 0;
	}
	*val = temp;

	return 1;
}



/*******************************************************************************
 * Getters and setters for register values
 ******************************************************************************/

/**
 * @brief Returns the lower threshold for proximity detection
 *
 * @return lower threshold
 */
uint8_t getProxIntLowThresh()
{
	uint8_t val;

	/* Read value from PILT register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_PILT, &val) ) {
		val = 0;
	}

	return val;
}

/**
 * @brief Sets the lower threshold for proximity detection
 *
 * @param[in] threshold the lower proximity threshold
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setProxIntLowThresh(uint8_t threshold)
{
	if(  I2C_WriteByte(APDS9960_I2C_ADDR,APDS9960_PILT, &threshold) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Returns the high threshold for proximity detection
 *
 * @return high threshold
 */
uint8_t getProxIntHighThresh()
{
	uint8_t val;

	/* Read value from PIHT register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_PIHT, &val) ) {
		val = 0;
	}

	return val;
}

/**
 * @brief Sets the high threshold for proximity detection
 *
 * @param[in] threshold the high proximity threshold
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setProxIntHighThresh(uint8_t threshold)
{
	if( I2C_WriteByte(APDS9960_I2C_ADDR,APDS9960_PIHT, &threshold) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Returns LED drive strength for proximity and ALS
 *
 * Value    LED Current
 *   0        100 mA
 *   1         50 mA
 *   2         25 mA
 *   3         12.5 mA
 *
 * @return the value of the LED drive strength. 0xFF on failure.
 */
uint8_t getLEDDrive()
{
	uint8_t val;

	/* Read value from CONTROL register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_CONTROL, &val) ) {
		return ERROR;
	}

	/* Shift and mask out LED drive bits */
	val = (val >> 6) & 0b00000011;

	return val;
}

/**
 * @brief Sets the LED drive strength for proximity and ALS
 *
 * Value    LED Current
 *   0        100 mA
 *   1         50 mA
 *   2         25 mA
 *   3         12.5 mA
 *
 * @param[in] drive the value (0-3) for the LED drive strength
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setLEDDrive(uint8_t drive)
{
	uint8_t val;

	/* Read value from CONTROL register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_CONTROL, &val) ) {
		return 0;
	}

	/* Set bits in register to given value */
	drive &= 0b00000011;
	drive = drive << 6;
	val &= 0b00111111;
	val |= drive;

	/* Write register value back into CONTROL register */
	if( I2C_WriteByte(APDS9960_I2C_ADDR,APDS9960_CONTROL, &val) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Returns receiver gain for proximity detection
 *
 * Value    Gain
 *   0       1x
 *   1       2x
 *   2       4x
 *   3       8x
 *
 * @return the value of the proximity gain. 0xFF on failure.
 */
uint8_t getProximityGain()
{
	uint8_t val;

	/* Read value from CONTROL register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_CONTROL, &val) ) {
		return ERROR;
	}

	/* Shift and mask out PDRIVE bits */
	val = (val >> 2) & 0b00000011;

	return val;
}

/**
 * @brief Sets the receiver gain for proximity detection
 *
 * Value    Gain
 *   0       1x
 *   1       2x
 *   2       4x
 *   3       8x
 *
 * @param[in] drive the value (0-3) for the gain
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setProximityGain(uint8_t drive)
{
	uint8_t val;

	/* Read value from CONTROL register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_CONTROL, &val) ) {
		return 0;
	}

	/* Set bits in register to given value */
	drive &= 0b00000011;
	drive = drive << 2;
	val &= 0b11110011;
	val |= drive;

	/* Write register value back into CONTROL register */
	if( I2C_WriteByte(APDS9960_I2C_ADDR,APDS9960_CONTROL, &val) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Returns receiver gain for the ambient light sensor (ALS)
 *
 * Value    Gain
 *   0        1x
 *   1        4x
 *   2       16x
 *   3       64x
 *
 * @return the value of the ALS gain. 0xFF on failure.
 */
uint8_t getAmbientLightGain()
{
	uint8_t val;

	/* Read value from CONTROL register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_CONTROL, &val) ) {
		return ERROR;
	}

	/* Shift and mask out ADRIVE bits */
	val &= 0b00000011;

	return val;
}

/**
 * @brief Sets the receiver gain for the ambient light sensor (ALS)
 *
 * Value    Gain
 *   0        1x
 *   1        4x
 *   2       16x
 *   3       64x
 *
 * @param[in] drive the value (0-3) for the gain
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setAmbientLightGain(uint8_t drive)
{
	uint8_t val;

	/* Read value from CONTROL register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_CONTROL, &val) ) {
		return 0;
	}

	/* Set bits in register to given value */
	drive &= 0b00000011;
	val &= 0b11111100;
	val |= drive;

	/* Write register value back into CONTROL register */
	if( I2C_WriteByte(APDS9960_I2C_ADDR,APDS9960_CONTROL, &val) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Get the current LED boost value
 *
 * Value  Boost Current
 *   0        100%
 *   1        150%
 *   2        200%
 *   3        300%
 *
 * @return The LED boost value. 0xFF on failure.
 */
uint8_t getLEDBoost()
{
	uint8_t val;

	/* Read value from CONFIG2 register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_CONFIG2, &val) ) {
		return ERROR;
	}

	/* Shift and mask out LED_BOOST bits */
	val = (val >> 4) & 0b00000011;

	return val;
}

/**
 * @brief Sets the LED current boost value
 *
 * Value  Boost Current
 *   0        100%
 *   1        150%
 *   2        200%
 *   3        300%
 *
 * @param[in] drive the value (0-3) for current boost (100-300%)
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setLEDBoost(uint8_t boost)
{
	uint8_t val;

	/* Read value from CONFIG2 register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_CONFIG2, &val) ) {
		return 0;
	}

	/* Set bits in register to given value */
	boost &= 0b00000011;
	boost = boost << 4;
	val &= 0b11001111;
	val |= boost;

	/* Write register value back into CONFIG2 register */
	if(  I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_CONFIG2, &val) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Gets proximity gain compensation enable
 *
 * @return 1 if compensation is enabled. 0 if not. 0xFF on error.
 */
uint8_t getProxGainCompEnable()
{
	uint8_t val;

	/* Read value from CONFIG3 register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_CONFIG3, &val) ) {
		return ERROR;
	}

	/* Shift and mask out PCMP bits */
	val = (val >> 5) & 0b00000001;

	return val;
}

/**
 * @brief Sets the proximity gain compensation enable
 *
 * @param[in] enable 1 to enable compensation. 0 to disable compensation.
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setProxGainCompEnable(uint8_t enable)
{
	uint8_t val;

	/* Read value from CONFIG3 register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_CONFIG3, &val) ) {
		return 0;
	}

	/* Set bits in register to given value */
	enable &= 0b00000001;
	enable = enable << 5;
	val &= 0b11011111;
	val |= enable;

	/* Write register value back into CONFIG3 register */
	if( I2C_WriteByte(APDS9960_I2C_ADDR,APDS9960_CONFIG3, &val) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Gets the current mask for enabled/disabled proximity photodiodes
 *
 * 1 = disabled, 0 = enabled
 * Bit    Photodiode
 *  3       UP
 *  2       DOWN
 *  1       LEFT
 *  0       RIGHT
 *
 * @return Current proximity mask for photodiodes. 0xFF on error.
 */
uint8_t getProxPhotoMask()
{
	uint8_t val;

	/* Read value from CONFIG3 register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_CONFIG3, &val) ) {
		return ERROR;
	}

	/* Mask out photodiode enable mask bits */
	val &= 0b00001111;

	return val;
}

/**
 * @brief Sets the mask for enabling/disabling proximity photodiodes
 *
 * 1 = disabled, 0 = enabled
 * Bit    Photodiode
 *  3       UP
 *  2       DOWN
 *  1       LEFT
 *  0       RIGHT
 *
 * @param[in] mask 4-bit mask value
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setProxPhotoMask(uint8_t mask)
{
	uint8_t val;

	/* Read value from CONFIG3 register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_CONFIG3, &val) ) {
		return 0;
	}

	/* Set bits in register to given value */
	mask &= 0b00001111;
	val &= 0b11110000;
	val |= mask;

	/* Write register value back into CONFIG3 register */
	if( I2C_WriteByte(APDS9960_I2C_ADDR,APDS9960_CONFIG3, &val) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Gets the entry proximity threshold for gesture sensing
 *
 * @return Current entry proximity threshold.
 */
uint8_t getGestureEnterThresh()
{
	uint8_t val;

	/* Read value from GPENTH register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_GPENTH, &val) ) {
		val = 0;
	}

	return val;
}

/**
 * @brief Sets the entry proximity threshold for gesture sensing
 *
 * @param[in] threshold proximity value needed to start gesture mode
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setGestureEnterThresh(uint8_t threshold)
{
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_GPENTH, &threshold) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Gets the exit proximity threshold for gesture sensing
 *
 * @return Current exit proximity threshold.
 */
uint8_t getGestureExitThresh()
{
	uint8_t val;

	/* Read value from GEXTH register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_GEXTH, &val) ) {
		val = 0;
	}

	return val;
}

/**
 * @brief Sets the exit proximity threshold for gesture sensing
 *
 * @param[in] threshold proximity value needed to end gesture mode
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setGestureExitThresh(uint8_t threshold)
{
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_GEXTH, &threshold) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Gets the gain of the photodiode during gesture mode
 *
 * Value    Gain
 *   0       1x
 *   1       2x
 *   2       4x
 *   3       8x
 *
 * @return the current photodiode gain. 0xFF on error.
 */
uint8_t getGestureGain()
{
	uint8_t val;

	/* Read value from GCONF2 register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_GCONF2, &val) ) {
		return ERROR;
	}

	/* Shift and mask out GGAIN bits */
	val = (val >> 5) & 0b00000011;

	return val;
}

/**
 * @brief Sets the gain of the photodiode during gesture mode
 *
 * Value    Gain
 *   0       1x
 *   1       2x
 *   2       4x
 *   3       8x
 *
 * @param[in] gain the value for the photodiode gain
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setGestureGain(uint8_t gain)
{
	uint8_t val;

	/* Read value from GCONF2 register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_GCONF2, &val) ) {
		return 0;
	}

	/* Set bits in register to given value */
	gain &= 0b00000011;
	gain = gain << 5;
	val &= 0b10011111;
	val |= gain;

	/* Write register value back into GCONF2 register */
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_GCONF2, &val) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Gets the drive current of the LED during gesture mode
 *
 * Value    LED Current
 *   0        100 mA
 *   1         50 mA
 *   2         25 mA
 *   3         12.5 mA
 *
 * @return the LED drive current value. 0xFF on error.
 */
uint8_t getGestureLEDDrive()
{
	uint8_t val;

	/* Read value from GCONF2 register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_GCONF2, &val) ) {
		return ERROR;
	}

	/* Shift and mask out GLDRIVE bits */
	val = (val >> 3) & 0b00000011;

	return val;
}

/**
 * @brief Sets the LED drive current during gesture mode
 *
 * Value    LED Current
 *   0        100 mA
 *   1         50 mA
 *   2         25 mA
 *   3         12.5 mA
 *
 * @param[in] drive the value for the LED drive current
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setGestureLEDDrive(uint8_t drive)
{
	uint8_t val;

	/* Read value from GCONF2 register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_GCONF2, &val) ) {
		return 0;
	}

	/* Set bits in register to given value */
	drive &= 0b00000011;
	drive = drive << 3;
	val &= 0b11100111;
	val |= drive;

	/* Write register value back into GCONF2 register */
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_GCONF2, &val) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Gets the time in low power mode between gesture detections
 *
 * Value    Wait time
 *   0          0 ms
 *   1          2.8 ms
 *   2          5.6 ms
 *   3          8.4 ms
 *   4         14.0 ms
 *   5         22.4 ms
 *   6         30.8 ms
 *   7         39.2 ms
 *
 * @return the current wait time between gestures. 0xFF on error.
 */
uint8_t getGestureWaitTime()
{
	uint8_t val;

	/* Read value from GCONF2 register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_GCONF2, &val) ) {
		return ERROR;
	}

	/* Mask out GWTIME bits */
	val &= 0b00000111;

	return val;
}

/**
 * @brief Sets the time in low power mode between gesture detections
 *
 * Value    Wait time
 *   0          0 ms
 *   1          2.8 ms
 *   2          5.6 ms
 *   3          8.4 ms
 *   4         14.0 ms
 *   5         22.4 ms
 *   6         30.8 ms
 *   7         39.2 ms
 *
 * @param[in] the value for the wait time
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setGestureWaitTime(uint8_t time)
{
	uint8_t val;

	/* Read value from GCONF2 register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_GCONF2, &val) ) {
		return 0;
	}

	/* Set bits in register to given value */
	time &= 0b00000111;
	val &= 0b11111000;
	val |= time;

	/* Write register value back into GCONF2 register */
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_GCONF2, &val) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Gets the low threshold for ambient light interrupts
 *
 * @param[out] threshold current low threshold stored on the APDS-9960
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t getLightIntLowThreshold(uint16_t *threshold)
{
	uint8_t val_byte;
	threshold = 0;

	/* Read value from ambient light low threshold, low byte register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_AILTL, &val_byte) ) {
		return 0;
	}
	*threshold = val_byte;

	/* Read value from ambient light low threshold, high byte register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_AILTH, &val_byte) ) {
		return 0;
	}
	threshold = threshold + ((uint16_t)val_byte << 8);

	return 1;
}

/**
 * @brief Sets the low threshold for ambient light interrupts
 *
 * @param[in] threshold low threshold value for interrupt to trigger
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setLightIntLowThreshold(uint16_t threshold)
{
	uint8_t val_low;
	uint8_t val_high;

	/* Break 16-bit threshold into 2 8-bit values */
	val_low = threshold & 0x00FF;
	val_high = (threshold & 0xFF00) >> 8;

	/* Write low byte */
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_AILTL, &val_low) ) {
		return 0;
	}

	/* Write high byte */
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_AILTH, &val_high) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Gets the high threshold for ambient light interrupts
 *
 * @param[out] threshold current low threshold stored on the APDS-9960
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t getLightIntHighThreshold(uint16_t *threshold)
{
	uint8_t val_byte;
	threshold = 0;

	/* Read value from ambient light high threshold, low byte register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_AIHTL, &val_byte) ) {
		return 0;
	}
	*threshold = val_byte;

	/* Read value from ambient light high threshold, high byte register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_AIHTH, &val_byte) ) {
		return 0;
	}
	threshold = threshold + ((uint16_t)val_byte << 8);

	return 1;
}

/**
 * @brief Sets the high threshold for ambient light interrupts
 *
 * @param[in] threshold high threshold value for interrupt to trigger
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setLightIntHighThreshold(uint16_t threshold)
{
	uint8_t val_low;
	uint8_t val_high;

	/* Break 16-bit threshold into 2 8-bit values */
	val_low = threshold & 0x00FF;
	val_high = (threshold & 0xFF00) >> 8;

	/* Write low byte */
	if(I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_AIHTL, &val_low) ) {
		return 0;
	}

	/* Write high byte */
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_AIHTH, &val_high) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Gets the low threshold for proximity interrupts
 *
 * @param[out] threshold current low threshold stored on the APDS-9960
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t getProximityIntLowThreshold(uint8_t *threshold)
{
	threshold = 0;

	/* Read value from proximity low threshold register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_PILT, threshold) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Sets the low threshold for proximity interrupts
 *
 * @param[in] threshold low threshold value for interrupt to trigger
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setProximityIntLowThreshold(uint8_t threshold)
{

	/* Write threshold value to register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR, APDS9960_PILT, &threshold) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Gets the high threshold for proximity interrupts
 *
 * @param[out] threshold current low threshold stored on the APDS-9960
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t getProximityIntHighThreshold(uint8_t *threshold)
{
	threshold = 0;

	/* Read value from proximity low threshold register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_PIHT, threshold) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Sets the high threshold for proximity interrupts
 *
 * @param[in] threshold high threshold value for interrupt to trigger
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setProximityIntHighThreshold(uint8_t threshold)
{

	/* Write threshold value to register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR, APDS9960_PIHT, &threshold) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Gets if ambient light interrupts are enabled or not
 *
 * @return 1 if interrupts are enabled, 0 if not. 0xFF on error.
 */
uint8_t getAmbientLightIntEnable()
{
	uint8_t val;

	/* Read value from ENABLE register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_ENABLE, &val) ) {
		return ERROR;
	}

	/* Shift and mask out AIEN bit */
	val = (val >> 4) & 0b00000001;

	return val;
}

/**
 * @brief Turns ambient light interrupts on or off
 *
 * @param[in] enable 1 to enable interrupts, 0 to turn them off
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setAmbientLightIntEnable(uint8_t enable)
{
	uint8_t val;

	/* Read value from ENABLE register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_ENABLE, &val) ) {
		return 0;
	}

	/* Set bits in register to given value */
	enable &= 0b00000001;
	enable = enable << 4;
	val &= 0b11101111;
	val |= enable;

	/* Write register value back into ENABLE register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR, APDS9960_ENABLE, &val) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Gets if proximity interrupts are enabled or not
 *
 * @return 1 if interrupts are enabled, 0 if not. 0xFF on error.
 */
uint8_t getProximityIntEnable()
{
	uint8_t val;

	/* Read value from ENABLE register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_ENABLE, &val) ) {
		return ERROR;
	}

	/* Shift and mask out PIEN bit */
	val = (val >> 5) & 0b00000001;

	return val;
}

/**
 * @brief Turns proximity interrupts on or off
 *
 * @param[in] enable 1 to enable interrupts, 0 to turn them off
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setProximityIntEnable(uint8_t enable)
{
	uint8_t val;

	/* Read value from ENABLE register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_ENABLE, &val) ) {
		return 0;
	}

	/* Set bits in register to given value */
	enable &= 0b00000001;
	enable = enable << 5;
	val &= 0b11011111;
	val |= enable;

	/* Write register value back into ENABLE register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR, APDS9960_ENABLE, &val) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Gets if gesture interrupts are enabled or not
 *
 * @return 1 if interrupts are enabled, 0 if not. 0xFF on error.
 */
uint8_t getGestureIntEnable()
{
	uint8_t val;

	/* Read value from GCONF4 register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_GCONF4, &val) ) {
		return ERROR;
	}

	/* Shift and mask out GIEN bit */
	val = (val >> 1) & 0b00000001;

	return val;
}

/**
 * @brief Turns gesture-related interrupts on or off
 *
 * @param[in] enable 1 to enable interrupts, 0 to turn them off
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setGestureIntEnable(uint8_t enable)
{
	uint8_t val;

	/* Read value from GCONF4 register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_GCONF4, &val) ) {
		return 0;
	}

	/* Set bits in register to given value */
	enable &= 0b00000001;
	enable = enable << 1;
	val &= 0b11111101;
	val |= enable;

	/* Write register value back into GCONF4 register */
	if( I2C_WriteByte(APDS9960_I2C_ADDR, APDS9960_GCONF4, &val) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Clears the ambient light interrupt
 *
 * @return 1 if operation completed successfully. 0 otherwise.
 */
uint8_t clearAmbientLightInt()
{
	uint8_t throwaway;
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_AICLEAR, &throwaway) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Clears the proximity interrupt
 *
 * @return 1 if operation completed successfully. 0 otherwise.
 */
uint8_t clearProximityInt()
{
	uint8_t throwaway;
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_PICLEAR, &throwaway) ) {
		return 0;
	}

	return 1;
}

/**
 * @brief Tells if the gesture state machine is currently running
 *
 * @return 1 if gesture state machine is running, 0 if not. 0xFF on error.
 */
uint8_t getGestureMode()
{
	uint8_t val;

	/* Read value from GCONF4 register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_GCONF4, &val) ) {
		return ERROR;
	}

	/* Mask out GMODE bit */
	val &= 0b00000001;

	return val;
}

/**
 * @brief Tells the state machine to either enter or exit gesture state machine
 *
 * @param[in] mode 1 to enter gesture state machine, 0 to exit.
 * @return 1 if operation successful. 0 otherwise.
 */
uint8_t setGestureMode(uint8_t mode)
{
	uint8_t val;

	/* Read value from GCONF4 register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR,APDS9960_GCONF4, &val) ) {
		return 0;
	}

	/* Set bits in register to given value */
	mode &= 0b00000001;
	val &= 0b11111110;
	val |= mode;

	/* Write register value back into GCONF4 register */
	if( I2C_ReadByte(APDS9960_I2C_ADDR, APDS9960_GCONF4, &val) ) {
		return 0;
	}

	return 1;
}


void APDS9960_Measurement(void)
{
	int ans=0;

	uint8_t proximity_data=0;
	uint16_t ambient_light =0;
	uint16_t red_light =0;
	uint16_t green_light =0;
	uint16_t blue_light =0;


	APDS9960_Init();
	APDS9960_ALS_RGB_Config();
	APDS9960_Proximity_Config();
	HAL_Delay(400);
	ans = APDS9960_ALS_RGB_Measure(&ambient_light, &red_light, &green_light, &blue_light);
	HAL_Delay(300);
	ans = APDS9960_ALS_RGB_Measure(&ambient_light, &red_light, &green_light, &blue_light);
	printf("%8d:%1d\r\n", ambient_light , ans);
	printf("%8d:%1d\r\n", red_light , ans);
	printf("%8d:%1d\r\n", green_light , ans);
	printf("%8d:%1d\r\n", blue_light , ans);
	HAL_Delay(200);
	APDS9960_Init();
	APDS9960_ALS_RGB_Config();
	APDS9960_Proximity_Config();
	HAL_Delay(100);
	ans = APDS9960_Proximity_Measure(&proximity_data);
	printf("%8d:%1d\r\n", proximity_data , ans);
	HAL_Delay(50);



}

void APDS9960_MeasurementFake(void)
{
	int ans=0;

	uint8_t proximity_data=0;
	uint16_t ambient_light =0;
	uint16_t red_light =0;
	uint16_t green_light =0;
	uint16_t blue_light =0;


	APDS9960_Init();
	APDS9960_ALS_RGB_Config();
	APDS9960_Proximity_Config();
	HAL_Delay(10);
	ans = APDS9960_ALS_RGB_Measure(&ambient_light, &red_light, &green_light, &blue_light);
	HAL_Delay(10);
	ans = APDS9960_ALS_RGB_Measure(&ambient_light, &red_light, &green_light, &blue_light);

	HAL_Delay(10);
	APDS9960_Init();
	APDS9960_ALS_RGB_Config();
	APDS9960_Proximity_Config();
	HAL_Delay(10);
	ans = APDS9960_Proximity_Measure(&proximity_data);





}


///*******************************************************************************
// * Raw I2C Reads and Writes
// ******************************************************************************/
//
///**
// * @brief Writes a single byte to the I2C device (no register)
// *
// * @param[in] val the 1-byte value to write to the I2C device
// * @return 1 if successful write operation. 0 otherwise.
// */
//uint8_t wireWriteByte(uint8_t &val)
//{
//    Wire.beginTransmission(APDS9960_I2C_ADDR);
//    Wire.write(&val);
//    if( Wire.endTransmission() != 0 ) {
//        return 0;
//    }
//
//    return 1;
//}
//
///**
// * @brief Writes a single byte to the I2C device and specified register
// *
// * @param[in] reg the register in the I2C device to write to
// * @param[in] val the 1-byte value to write to the I2C device
// * @return 1 if successful write operation. 0 otherwise.
// */
//uint8_t wireWriteDataByte(uint8_t reg, uint8_t &val)
//{
//    Wire.beginTransmission(APDS9960_I2C_ADDR);
//    Wire.write(reg);
//    Wire.write(&val);
//    if( Wire.endTransmission() != 0 ) {
//        return 0;
//    }
//
//    return 1;
//}
//
///**
// * @brief Writes a block (array) of bytes to the I2C device and register
// *
// * @param[in] reg the register in the I2C device to write to
// * @param[in] val pointer to the beginning of the data byte array
// * @param[in] len the length (in bytes) of the data to write
// * @return 1 if successful write operation. 0 otherwise.
// */
//uint8_t wireWriteDataBlock(  uint8_t reg,
//                                        uint8_t *val,
//                                        unsigned int len)
//{
//    unsigned int i;
//
//    Wire.beginTransmission(APDS9960_I2C_ADDR);
//    Wire.write(reg);
//    for(i = 0; i < len; i++) {
//        Wire.beginTransmission(val[i]);
//    }
//    if( Wire.endTransmission() != 0 ) {
//        return 0;
//    }
//
//    return 1;
//}
//
///**
// * @brief Reads a single byte from the I2C device and specified register
// *
// * @param[in] reg the register to read from
// * @param[out] the value returned from the register
// * @return 1 if successful read operation. 0 otherwise.
// */
//uint8_t wireReadDataByte(uint8_t reg, uint8_t &&val)
//{
//
//    /* Indicate which register we want to read from */
//    if (!wireWriteByte(reg)) {
//        return 0;
//    }
//
//    /* Read from register */
//    Wire.requestFrom(APDS9960_I2C_ADDR, 1);
//    while (Wire.available()) {
//        val = Wire.read();
//    }
//
//    return 1;
//}
//
///**
// * @brief Reads a block (array) of bytes from the I2C device and register
// *
// * @param[in] reg the register to read from
// * @param[out] val pointer to the beginning of the data
// * @param[in] len number of bytes to read
// * @return Number of bytes read. -1 on read error.
// */
//int wireReadDataBlock(   uint8_t reg,
//                                        uint8_t *val,
//                                        unsigned int len)
//{
//    unsigned char i = 0;
//
//    /* Indicate which register we want to read from */
//    if (!wireWriteByte(reg)) {
//        return -1;
//    }
//
//    /* Read block data */
//    Wire.requestFrom(APDS9960_I2C_ADDR, len);
//    while (Wire.available()) {
//        if (i >= len) {
//            return -1;
//        }
//        val[i] = Wire.read();
//        i++;
//    }
//
//    return i;
//}



