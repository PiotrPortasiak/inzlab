/*
 * gpio_map.c
 *
 *  Created on: 30.11.2018
 *      Author: piotr
 */

#include "gpio_map.h"

const sensor_gpio_t sensor[SENSORS_ADC_N+SENSORS_I2C_N] = {
		{IR1_PWM_GPIO_Port, IR1_PWM_Pin},
		{IR2_PWM_GPIO_Port, IR2_PWM_Pin},
		{IR3_PWM_GPIO_Port, IR3_PWM_Pin},
		{SH1_ON_GPIO_Port, SH1_ON_Pin},
		{SH2_ON_GPIO_Port, SH2_ON_Pin},
		{EN1_GPIO_Port, EN1_Pin},
		{EN2_GPIO_Port, EN2_Pin},
		{EN3_GPIO_Port, EN3_Pin}
};
