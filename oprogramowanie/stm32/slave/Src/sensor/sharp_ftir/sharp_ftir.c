/*!
   \file   sharp_ftir.c
   \author Piotr Portasiak
   \brief  This file provides functions which perform measurements
   	   	   using SHARP and FTIR sensors.
 */

/* Includes --------------------------------------------------------*/
#include "sharp_ftir/sharp_ftir.h"

FlagStatus adc1Ready;
uint16_t adc1Buffer[SENSORS_ADC_N];

/*
  \brief  Regular conversion complete callback in non blocking mode
  \param  hadc pointer to a ADC_HandleTypeDef structure that contains
          the configuration information for the specified ADC.
  \retval None
  */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	/* Check whether a ADC conversion with DMA
	 * for the group of channels is complete */
	if(hadc->Instance == ADC1)
	{
		adc1Ready = SET;
	}
}

/*!
  \brief Perform ADC measurement using selected sensor
  \note  Function can be also used to perform differential measurement for FTIR sensors.
  \param GPIO_Port specifies the GPIO_Port of sensor
  \param GPIO_Pin specifies the GPIO_Pin of sensor
  \param PinState specifies the state to be assigned to the selected sensor.
         This parameter can be one of the FunctionalState enum values:
         \arg DISABLE: to clear the port pin
         \arg ENABLE: to set the port pin
  \param Timeout timeout for sensor initialization
  \return Error return some error codes as below
  		  \arg 1: start measurement error
          \arg 2: stop measurement error
 */

void ADC_Measurement(uint8_t id, FunctionalState PinState, uint16_t Timeout)
{

	/* Consider the fact that adc1Buffer array is indexed from 0 up to SENSORS_ADC_N-1 */
	assert_param(id>0 && id<=SENSORS_ADC_N);

	/* Decrement id */
	id--;

	/* Measurement status definition */
	uint8_t status = 0;

	/* Turn on the power for sensor */
	HAL_GPIO_WritePin(sensor[id].gpio_port, sensor[id].gpio_pin, PinState);

	/* Wait for sensor power-up */
	HAL_Delay(Timeout);

	/* Start measurement */
	if (HAL_ADC_Start_DMA(&hadc1, (uint32_t*) adc1Buffer, 5) != HAL_OK)
	{
		/* An error occurred, while starting measurement */
		Error_Handler();
		status = 1;
	}
	/* Wait for sensor response */
	HAL_Delay(Timeout);

	/* Wait for measurement complete status flag */
	while(adc1Ready!=SET);
	adc1Ready=RESET;

	/* Stop measurement */
	if (HAL_ADC_Stop_DMA(&hadc1) != HAL_OK)
	{
		/* An error occurred, while stopping measurement */
		Error_Handler();
		status = 2;
	}

	/* Turn off sensor */
	HAL_GPIO_WritePin(sensor[id].gpio_port, sensor[id].gpio_pin, DISABLE);

	/* Process the measurement result */
	printf("%8d:%1d\r\n", adc1Buffer[id], status);
	/* Probably not needed anymore */
	HAL_Delay(100);
}



