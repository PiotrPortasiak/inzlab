/*!
   \file   measurement.c
   \author Piotr Portasiak
   \brief  This file provides functions which perform measurements
   	   	   using many different sensors
 */

#include "measurement.h"
#include "workaround.h"
/*!
  \brief Measurement using selected sensor

  Measurement using selected sensor
  specified by sensor_id
  \param[in] sensor_id sensor identificator
  \retval None
 */
void Measurement_Perform(sensor_id_t sensor_id)
{



	switch(sensor_id)
	{
	case IR1:
	case IR2:
	case IR3:
		ADC_Measurement(sensor_id, ENABLE, FTIR_TIMEOUT);
		ADC_Measurement(sensor_id, DISABLE, FTIR_TIMEOUT);
		break;
	case SH1:
	case SH2:
		ADC_Measurement(sensor_id, ENABLE, SHARP_TIMEOUT);
		ADC_Measurement(sensor_id, DISABLE, SHARP_TIMEOUT);
		break;
	case TF1:
		VL53L0X_Measurement();
		break;
	case TF2:
		VL6180X_Measurement();
		break;
	case TF3:
		APDS9960_Measurement();
		break;
	case IDN:
	{
		printf("----------|\r\n");

		//I2C_ClearBusyFlagErratum(&hi2c1,10);
		/* IR1-IR3 differential measurements
		 * it means when IR transmitter is ON/OFF
		 * we can reduce ambient light impact */
		for (uint8_t id=IR1; id<=IR3; id++)
			//for (uint8_t state=SET; state>RESET; state)
		{
			ADC_Measurement(id, SET, FTIR_TIMEOUT);
			ADC_Measurement(id, RESET, FTIR_TIMEOUT);
		}

		/* SH1-SH2 measurement */
		for (uint8_t id=SH1; id<=SH2; id++)
			//for (uint8_t state=SET; state>RESET; state--)
		{
				ADC_Measurement(id, SET, SHARP_TIMEOUT);
				ADC_Measurement(id, RESET, SHARP_TIMEOUT);
		}
		/* VL53L0X Measurement */
		//VL53L0X_MeasurementFake();
		//I2C_ClearBusyFlagErratum(&hi2c1,10);
		//VL53L0X_MeasurementFake();
		APDS9960_MeasurementFake();
		VL53L0X_Measurement();
		//I2C_ClearBusyFlagErratum(&hi2c1,10);
		/* VL6180X Measurement(); */
		VL6180X_Measurement();
		/* APDS9960_Measurement */
		APDS9960_Measurement();

		printf("----------O\r\n");
	} break;

	}

}

/*!
  \brief Laser Pointer

  Manages Laser Pointer device
  \param[in] state Device state (DISABLED=0, ENABLED=1)
  \retval None
 */
void Laser_Pointer(FunctionalState state)
{
	HAL_GPIO_WritePin(LASER_ON_GPIO_Port,LASER_ON_Pin, state);
}
