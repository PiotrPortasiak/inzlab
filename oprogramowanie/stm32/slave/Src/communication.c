/*!
   \file   communication.c
   \author Piotr Portasiak
   \brief  This file provides functions which describe the communication.
 */

/* Includes --------------------------------------------------------*/
#include "communication.h"

/* External variables ----------------------------------------------*/
extern DMA_HandleTypeDef hdma_usart1_rx;
extern DMA_HandleTypeDef hdma_usart2_rx;

/* Local variables -------------------------------------------------*/
uint8_t rx1_recv = RESET;  /**< recv from MASTER flag  */
uint8_t rx1_buffer[BUFFER_SIZE] = {0};  /**< MASTER rx_buffer  */
uint32_t rx1_len = 0;  /**< how many bytes recv from MASTER */

uint8_t rx2_recv = RESET;  /**< recv from USER flag  */
uint8_t rx2_buffer[BUFFER_SIZE] = {0};  /**< USER rx_buffer  */
uint32_t rx2_len = 0;  /**< how many bytes recv from USER */

/* Functions -------------------------------------------------------*/

/*!
  \brief Communication initialization
  \param None
  \retval None
 */
void Communication_Init(void)
{
	/* Enable/Disable interrupts from MASTER (USART1) */
	__HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);
	__HAL_DMA_DISABLE_IT (&hdma_usart1_rx, DMA_IT_TC);
	__HAL_DMA_DISABLE_IT (&hdma_usart1_rx, DMA_IT_HT);

	/* Start listen from MASTER */
	HAL_UART_Receive_DMA(&huart1, rx1_buffer, sizeof(rx1_buffer));

	/* Enable/Disable interrupts from USER (USART2) */
	__HAL_UART_ENABLE_IT(&huart2, UART_IT_IDLE);
	__HAL_DMA_DISABLE_IT (&hdma_usart2_rx, DMA_IT_TC);
	__HAL_DMA_DISABLE_IT (&hdma_usart2_rx, DMA_IT_HT);

	/* Start listen from USER */
	HAL_UART_Receive_DMA(&huart2, rx2_buffer, sizeof(rx2_buffer));
}

/*!
   \brief USART_RX idle line detection callback.
   \param[in,out] huart pointer to a UART_HandleTypeDef structure that contains
 				the configuration information for the specified UART module.
   \retval None
 */
void HAL_UART_RxIdleCallback(UART_HandleTypeDef *huart)
{
	if(huart->Instance == USART1)
	{
		rx1_recv=SET;
	}
	if(huart->Instance == USART2)
	{
		rx2_recv=SET;
	}
}

/*!
  \brief Communication Perform
  \param None
  \retval None
 */
void Communication_Perform(uint8_t *start_measurement, uint8_t *sensor_id)
{
	/* Check whether a frame of bytes received from MASTER (blocking mode). */
	if(rx1_recv)
	{
		/* Calculate how many bytes received in the frame */
		rx1_len = sizeof(rx1_buffer)-hdma_usart1_rx.Instance->NDTR;

		/* Check whether the packet contains at least 2 bytes
		 * and start recognize commands for SLAVE (frame parsing). */
		if(rx1_len>=2)
		{
			/* Recognize commands */
			if(rx1_buffer[0]=='P')
			{
				*start_measurement = SET;
				*sensor_id = rx1_buffer[1];
			}
			else
			{
				/* If command is not recognized do something else */
				printf("CommandID not recognized\r\n");
			}
		}

		/* Reset the frame received from MASTER flag
		 * and start listen from USER (blocking mode). */
		rx1_recv=RESET;
		HAL_UART_Receive_DMA(&huart1, rx1_buffer, sizeof(rx1_buffer));
	}

	/* Check whether a frame of bytes received from USER (blocking mode). */
	if(rx2_recv)
	{
		/* Calculate how many bytes received in the frame */
		rx2_len = sizeof(rx2_buffer)-hdma_usart2_rx.Instance->NDTR;

		/* Check whether the packet contains at least 1 byte
		 * and send them back to USER */
		if(rx2_len)
		{
			HAL_UART_Transmit(&huart2, rx2_buffer, rx2_len, 0xFFFF);
		}

		/* Reset the frame received from USER flag
		 * and start listen from USER (blocking mode). */
		rx2_recv=RESET;
		HAL_UART_Receive_DMA(&huart2, rx2_buffer, sizeof(rx2_buffer));
	}

}

/*!
  \brief Implementation of printf() function

  This is the custom implementation of printf() function
  \param[in] file
  \param[in] ptr
  \param[in] len
  \retval len
 */
int _write (int file, char *ptr, int len)
{
	/* Send message to MASTER using printf() */
	HAL_UART_Transmit(&huart1, (uint8_t *) ptr, len, 0xFFFF);
	return len;
}
