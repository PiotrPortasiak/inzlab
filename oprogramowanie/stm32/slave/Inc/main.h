/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define FT3_ADC_Pin GPIO_PIN_0
#define FT3_ADC_GPIO_Port GPIOC
#define FT2_ADC_Pin GPIO_PIN_1
#define FT2_ADC_GPIO_Port GPIOC
#define SH2_ON_Pin GPIO_PIN_2
#define SH2_ON_GPIO_Port GPIOC
#define SH1_ON_Pin GPIO_PIN_3
#define SH1_ON_GPIO_Port GPIOC
#define LASER_ON_Pin GPIO_PIN_0
#define LASER_ON_GPIO_Port GPIOA
#define SH2_ADC_Pin GPIO_PIN_1
#define SH2_ADC_GPIO_Port GPIOA
#define uRX_Pin GPIO_PIN_2
#define uRX_GPIO_Port GPIOA
#define uTX_Pin GPIO_PIN_3
#define uTX_GPIO_Port GPIOA
#define SH1_ADC_Pin GPIO_PIN_4
#define SH1_ADC_GPIO_Port GPIOA
#define INT1_Pin GPIO_PIN_5
#define INT1_GPIO_Port GPIOA
#define INT2_Pin GPIO_PIN_6
#define INT2_GPIO_Port GPIOA
#define INT3_Pin GPIO_PIN_7
#define INT3_GPIO_Port GPIOA
#define FT1_ADC_Pin GPIO_PIN_0
#define FT1_ADC_GPIO_Port GPIOB
#define IR1_PWM_Pin GPIO_PIN_10
#define IR1_PWM_GPIO_Port GPIOB
#define EN3_Pin GPIO_PIN_12
#define EN3_GPIO_Port GPIOB
#define D1_Pin GPIO_PIN_7
#define D1_GPIO_Port GPIOC
#define sTX_Pin GPIO_PIN_9
#define sTX_GPIO_Port GPIOA
#define sRX_Pin GPIO_PIN_10
#define sRX_GPIO_Port GPIOA
#define EN2_Pin GPIO_PIN_11
#define EN2_GPIO_Port GPIOA
#define EN1_Pin GPIO_PIN_12
#define EN1_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
#define IR2_PWM_Pin GPIO_PIN_4
#define IR2_PWM_GPIO_Port GPIOB
#define IR3_PWM_Pin GPIO_PIN_5
#define IR3_PWM_GPIO_Port GPIOB
#define D0_Pin GPIO_PIN_6
#define D0_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
