/*!
   \file   measurement.h
   \author Piotr Portasiak
   \brief  This file provides functions which perform measurements
   	   	   using many different sensors
 */

#ifndef SENSOR_MEASUREMENT_H_
#define SENSOR_MEASUREMENT_H_

#include "gpio.h"
#include "i2c.h"
#include "apds9960/apds9960.h"
#include "vl6180x/vl6180x_api.h"
#include "vl53l0x/vl53l0x_stm_api.h"
#include "sharp_ftir/sharp_ftir.h"

/*!
  Enumeration of sensors ID
 */
typedef enum {
	IDN = 0,	/**< ALL sensors (compatibility) */
	IR1,		/**< FTIR1 */
	IR2, 		/**< FTIR2 */
	IR3, 		/**< FTIR3 */
	SH1, 		/**< SHARP1 */
	SH2, 		/**< SHARP2 */
	TF1, 		/**< VL53L0X */
	TF2, 		/**< VL6180X */
	TF3  		/**< APDS9960 */

} sensor_id_t;

void Measurement_Perform(sensor_id_t sensor_id);
void Laser_Pointer(FunctionalState state);

#endif /* SENSOR_MEASUREMENT_H_ */
