/*!
   \file   sharp_ftir.h
   \author Piotr Portasiak
   \brief  This file provides functions which perform measurements
   	   	   using SHARP and FTIR sensors.
 */

#ifndef SHARP_FTIR_H_
#define SHARP_FTIR_H_

/* Includes --------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "gpio.h"
#include "adc.h"
#include "gpio_map.h"

/* Macros ----------------------------------------------------------*/

#define FTIR_TIMEOUT 50 /**< FTIR_TIMEOUT in msec */
#define SHARP_TIMEOUT 50 /**< SHARP_TIMEOUT in msec */

/* Functions headers -----------------------------------------------*/
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc);
void ADC_Measurement(uint8_t id, FunctionalState PinState, uint16_t Timeout);

#endif /* SHARP_FTIR_H_ */
