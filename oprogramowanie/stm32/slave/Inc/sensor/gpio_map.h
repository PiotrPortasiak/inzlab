/*
 * gpio_map.h
 *
 *  Created on: 23.09.2018
 *      Author: piotr
 */

#ifndef SENSOR_GPIO_MAP_H_
#define SENSOR_GPIO_MAP_H_

#define SENSORS_ADC_N 5 /**< number of ADC sensors*/
#define SENSORS_I2C_N 3 /**< number of I2C sensors*/

#include "gpio.h"

/*!
 Structure which represents stepper motor
 */
typedef struct {
	/*!
	  \name DIR
	 */
	/*\{*/
	GPIO_TypeDef *gpio_port;		/**< Direction GPIO_port */
	uint16_t gpio_pin;			/**< Direction GPIO_pin */
	/*\}*/
} sensor_gpio_t;

//extern const sensor_gpio_t sensor[SENSORS_ADC_N+SENSORS_I2C_N] = {
//		{IR1_PWM_GPIO_Port, IR1_PWM_Pin},
//		{IR2_PWM_GPIO_Port, IR2_PWM_Pin},
//		{IR3_PWM_GPIO_Port, IR3_PWM_Pin},
//		{SH1_ON_GPIO_Port, SH1_ON_Pin},
//		{SH2_ON_GPIO_Port, SH2_ON_Pin},
//		{EN1_GPIO_Port, EN1_Pin},
//		{EN2_GPIO_Port, EN2_Pin},
//		{EN3_GPIO_Port, EN3_Pin}
//};

extern const sensor_gpio_t sensor[SENSORS_ADC_N+SENSORS_I2C_N];

#endif /* SENSOR_GPIO_MAP_H_ */
