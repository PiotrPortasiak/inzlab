/*
 * workaround.h
 *
 *  Created on: 14.12.2018
 *      Author: piotr
 */

#ifndef SENSOR_WORKAROUND_H_
#define SENSOR_WORKAROUND_H_

#include "gpio.h"
#include "i2c.h"

#define I2C1_SCL_Pin GPIO_PIN_8
#define I2C1_SCL_GPIO_Port GPIOB
#define I2C1_SDA_Pin GPIO_PIN_9
#define I2C1_SDA_GPIO_Port GPIOB



uint8_t wait_for_gpio_state_timeout(GPIO_TypeDef *port, uint16_t pin, GPIO_PinState state, uint32_t timeout);
void I2C_ClearBusyFlagErratum(I2C_HandleTypeDef* handle, uint32_t timeout);

#endif /* SENSOR_WORKAROUND_H_ */
